\begin{frame}[c]

\begin{center}
{\fontsize{50}{60} \thankfont \Navy{Contribution 2:}}

\vskip 20pt

{\fontsize{25}{30} \Navy{\rm  Unification of PITs}}

\end{center}
\end{frame}


\begin{frame}
\frametitle{Heart of black-box PITs}

\begin{block}{Black-box PITs}
  Given access to a black-box computing $f$, check if $f \equiv 0$.\\

  \only<2->{Construct a ``small'' set of evaluations such that $f$
    evaluates to a non-zero value on at least one of them. }
\end{block}


\only<3->{
\begin{lemma}[\Blue{[Schwartz-Zippel]}]
  Let $S \subseteq \F$ of size greater than $d$. Then for every
  non-zero $n$-variate degree $d$ polynomial $f$, we have that
  $f(a_1,\dots, a_n) \neq 0$ for some $a_1,\dots, a_n \in S$. 

  \only<4->{\Gray{That is, there is a hitting set of size $(d+1)^n$ for degree $d$ polynomials.}}
\end{lemma}
}


\only<5->{
\begin{block}{Variable Reduction}
  A map $\Phi:\F[x_1,\dots, x_n] \mapsto \F[y_1,\dots, y_k]$ such that
  $f \neq 0$ if and only if $\Phi(f) \neq 0$.
\end{block}
}
\end{frame}



\begin{frame}
\frametitle{A motivating example}

\begin{eqnarray*}
C  & = & \sum_{i} \prod_{j} \only<1-6>{\ell_{ij}}\only<7->{\Red{f_{ij}}}\White{random whitespace that is long long long} \uncover<2->{\\
\textrm{rank}(C) & \stackrel{\text{def}}{=} & \only<1-7>{\textrm{dim}\inbrace{\ell_{ij}}}\only<8->{\Red{\textrm{algRank}\inbrace{f_{ij}}}}\\
 &  & {\small \text{the maximum number of \only<1-7>{linearly}\only<8->{\Red{algebraically}} independent \only<1-7>{$\ell_{ij}$}\only<8->{$f_{ij}$}'s}}}
\end{eqnarray*}

\only<3->{
If $\textrm{rank}(C)$ is ``small'' (say less than $k$):
\only<4->{
\begin{enumerate}
\only<4->{\item Construct a \only<1-8>{linear transformation}\only<9->{\Red{homomorphism}} $\Psi:\F[x_{[n]}] \mapsto
  \F[y_{[k]}]$ such that \only<1-8>{$\textrm{dim}\inbrace{\ell_{ij}} = \textrm{dim}\inbrace{\Psi(\ell_{ij})}$}
  \only<9->{\Red{$\textrm{algRank}\inbrace{f_{ij}} = \textrm{algRank}\inbrace{\Psi(f_{ij})}$}}.}
\only<5->{\item Show that this preserves non-zeroness of $C$.}
\only<6->{\item Use \Blue{[Schwartz-Zippel]} on $\Psi(C)$ to get a hitting set of size $(\only<1-8>{d}\only<9->{\Red{\poly(d)}}+1)^{k}$.}
\end{enumerate}
}
}
\end{frame}

\begin{frame}
\frametitle{Formal definitions}

\begin{definition}
  $\inbrace{f_1,\cdots, f_m}$ are \Blue{algebraically independent} if there is no non-trivial polynomial relation between them. That is, 
  $$
  H(f_1,\cdots, f_m) = 0 \quad\Longleftrightarrow\quad H = 0
  $$
\end{definition}
\pause

\vskip 20pt

\begin{definition}
The \Blue{algebraic rank ($\mathrm{algRank}$)} of $\inbrace{f_1,\cdots, f_m}$ is the size of the largest algebraically independent subset.
\end{definition}


\end{frame}

\begin{frame}[c]
  \frametitle{The Jacobian}

  $$
  \mathcal{J}_{x_1,\cdots, x_n}(f_1,\cdots, f_m) = \insquare{\begin{array}{ccc}
      \parderiv{f_1}{x_1} & \cdots & \parderiv{f_1}{x_n}\\
      \vdots & \ddots & \vdots \\
      \parderiv{f_m}{x_1} & \cdots & \parderiv{f_m}{x_n}\\
  \end{array}}_{m\times n}
  $$

\onslide<2->{ \begin{theorem}[Jacobi Criterion] If $\mathrm{char}(\F) = 0$ or
    ``large enough'',
    $$\mathrm{algRank}\inbrace{f_1,\cdots, f_m} \quad=\quad \mathrm{rank}(\mathcal{J}(f_1,\cdots, f_m))$$
  \end{theorem}
}
\end{frame}


\begin{frame}
  \frametitle{The Variable Reduction}
  \begin{center}
    \begin{tikzpicture}[scale=0.8,transform shape]
      \node (calJ) at (0,0) {$\mathcal{J}(f_1,\cdots, f_m) = $};
      \draw[fill=black!50] (1.5,-1) rectangle +(4,2);
      \draw[thick] (1.6,1.1) -- (1.4,1.1) -- (1.4,-1.1) -- (1.6,-1.1);
      \draw[thick] (5.4,1.1) -- (5.6,1.1) -- (5.6,-1.1) -- (5.4,-1.1);
      
      \uncover<2->{
        \draw[fill=black!20] (1.5,-0.3) rectangle +(1.3,1.3);}
      
      \uncover<3->{
        \node (J) at (2.15,-2.5) {$J = \abs{\begin{array}{ccc}
              \pder{f_1}{x_1} & \cdots & \pder{f_1}{x_k}\\
              \vdots & \ddots & \vdots \\
              \pder{f_k}{x_1} & \cdots & \pder{f_k}{x_k}
            \end{array}}$}
        edge[stealth-, very thick] (2.15,-0.3);
      }
      \uncover<4->{
        \node at (7,-2.5) {{\large Preserve this determinant}}
        edge[-stealth, thin] (J);
    }
    \end{tikzpicture}
  \end{center}
  \onslide<4->{
    \begin{lemma}[Composition Lemma]
      \only<4>{ If we can preserve non-zeroness of $J$, then we can
        then we can preserve non-zeroness of any $C(f_1,\dots, f_m)$ with ``few'' additional variables.
      }\only<5->{ Let $\Phi$ be a map such that $\Phi(\ J\ ) \neq
        0$. Then the map $\Psi$ preserves non-zeroness of $C(f_1,\cdots,
          f_m)$:
      $$\Psi:x_i\quad\mapsto\quad \sum_{j=1}^k y_j t^{ij} \quad+\quad \Phi(x_i)$$}
    \end{lemma}
  }
\end{frame}

\begin{frame}[c]
\frametitle{Unification: A meta-statement}

Almost all the classes for which we know polynomial time black-box
PITs have inside them \Red{``simple'' jacobian minors}.

\end{frame}

\begin{frame}
  \frametitle{Bounded read formulae}

    \begin{center}
    \tikzstyle{every node} = [minimum size=33pt]
    \tikzstyle{gate}=[circle,draw=black!50,thick]
    \tikzstyle{leafa}=[circle,draw=red,fill=black!10,text=red,thick]
    \tikzstyle{leaf}=[circle,draw=black!50,fill=black!10,thick]
    \begin{tikzpicture}[scale=0.4, transform shape]
      \only<1>{\node[leaf] (l1) at (-11,0) {{\LARGE $x_1$}};}
      \only<2->{\node[leafa] (l1) at (-11,0) {{\LARGE $x_1$}};}
      \node[leaf] (l2) at (-9,0) {{\LARGE $x_2$}};
      \node[leaf] (l3) at (-7,0) {{\LARGE $x_3$}};
      \node[leaf] (l4) at (-5,0) {{\LARGE $x_4$}};
      \node[leaf] (l5) at (-3,0) {{\LARGE $x_5$}};
      \node[leaf] (l6) at (-1,0) {{\LARGE $x_6$}};
      \only<1,2>{\node[leaf] (l7) at (1,0) {{\LARGE $x_{7}$}};}
      \only<3->{\node[leafa] (l7) at (1,0) {{\LARGE $x_{1}$}};}
      \node[leaf] (l8) at (3,0) {{\LARGE $x_8$}};
      \node[leaf] (l9) at (5,0) {{\LARGE $x_9$}};
      \only<1>{\node[leaf] (l10) at (7,0) {{\LARGE $x_{10}$}};}
      \only<2->{\node[leafa] (l10) at (7,0) {{\LARGE $x_{1}$}};}
      \node[leaf] (l11) at (9,0) {{\LARGE $x_{11}$}};
      \node[leaf] (l12) at (11,0) {{\LARGE $x_{12}$}};
      \node[gate] (p1) at (-10,2) {{\Large $+$}}
      edge[<-] (l1)
      edge[<-] (l2);
      \node[gate] (p2) at (-6,2) {{\Large $+$}}
      edge[<-] (l3)
      edge[<-] (l4);
      \node[gate] (p3) at (-2,2) {{\Large $+$}}
      edge[<-] (l5)
      edge[<-] (l6);
      \node[gate] (p4) at (2,2) {{\Large $+$}}
      edge[<-] (l7)
      edge[<-] (l8);
      \node[gate] (p5) at (6,2) {{\Large $+$}}
      edge[<-] (l9)
      edge[<-] (l10);
      \node[gate] (p6) at (10,2) {{\Large $+$}}
      edge[<-] (l11)
      edge[<-] (l12);
      \node[gate] (t1) at (-8,4) {{\Large $\times$}}
      edge[<-] (p1)
      edge[<-] (p2);
      \node[gate] (t2) at (0,4) {{\Large $\times$}}
      edge[<-] (p3)
      edge[<-] (p4);
      \node[gate] (t3) at (8,4) {{\Large $\times$}}
      edge[<-] (p5)
      edge[<-] (p6);
    \node[gate] at (0,6) {{\Large $+$}}
    edge[<-] (t1)
    edge[<-] (t2)
    edge[<-] (t3);
    \end{tikzpicture}
    \end{center}
    \begin{center}
      Read-\only<1>{$1$}\only<2>{$2$}\only<3>{$3$} formula
    \end{center}
\end{frame}

\begin{frame}
  \frametitle{Read-$k$ depth-$4$ formulae}
  \begin{center}
    \tikzstyle{every node} = [minimum size=35pt]
    \tikzstyle{gate}=[circle,draw=black!50,thick]
    \tikzstyle{leaf}=[circle,draw=black!50,fill=black!10,thick]
    \begin{tikzpicture}[scale=0.5, transform shape]
      \node[gate] (root) at (0,0) {{\Large $+$}};
      
      \draw[->,thin,draw=red] (root) +(180:1.3cm) arc
      (180:360:1.3cm);
      \node () at (1.7,0) {\only<1-2>{$m$}\only<3>{$2k$}\only<4->{$k$}};
      \node[below of=root, node distance=2cm] (dot1) {{\Huge $\cdot$}};
      \node[left of=dot1, node distance=1cm] {{\Huge $\cdot$}};
      \node[right of=dot1, node distance=1cm] {{\Huge $\cdot$}};
      
      \only<1-2>{
        \node[gate, left of=dot1, node distance=4cm] (T1) {{\Large $\times$}}
        edge[->] (root);
        \node[gate, right of=dot1, node distance=4cm] (Tm) {{\Large $\times$}}
        edge[->] (root);
      }
      \only<3->{
        \node[gate, left of=dot1, node distance=2.5cm] (T1) {{\Large $\times$}}
        edge[->] (root);
        \node[gate, right of=dot1, node distance=2.5cm] (Tm) {{\Large $\times$}}
        edge[->] (root);
      }
      \node[below of=T1, node distance=2cm] (dot2a) {{\Large $\cdots$}};
      \node[leaf, left of=dot2a, node distance=1cm] (f11) {{\large $f_{1,1}$}}
      edge[->] (T1);
      \node[leaf, right of=dot2a, node distance=1cm] (f1d) {{\large $f_{1,d}$}}
      edge[->] (T1);
      
      \node[below of=Tm, node distance=2cm] (dot2b) {{\Large $\cdots$}};
      \node[leaf, left of=dot2b, node distance=1cm] (fm1) {{\large \only<1-2>{$f_{m,1}$}\only<3>{$f_{2k,1}$}\only<4->{$f_{k,1}$}}}
      edge[->] (Tm);
      \node[leaf, right of=dot2b, node distance=1cm] (fmd) {{\large \only<1-2>{$f_{m,d}$}\only<3>{$f_{2k,d}$}\only<4->{$f_{k,d}$}}}
      edge[->] (Tm);
      
      \only<4->{
        \node[above left of=T1, node distance=1cm] {{\Large $T_1$}};
        \node[above right of=Tm, node distance=1cm] {{\Large $T_{k}$}};
      }
    \end{tikzpicture}
    
  \end{center}
  \only<2-3>{
  \begin{observation}
    If $C(x_1,\cdots, x_n) \neq 0$, then there exists an $i$ such that 
    $$ 
    C(x_1,\cdots,x_i + 1, \cdots, x_n) - C(x_1,\cdots, x_i, \cdots, x_n) \neq 0
    $$
  \end{observation}
  \Gray{In fact, any $x_i$ that $C$ non-trivially depends on.}
  }
  \only<5->{
        \begin{tikzpicture}[scale=0.8,transform shape]
          \only<5>{
          \node (calJ) at (0,0) {$\mathcal{J}(T_1,\cdots, T_k) = $};
          \draw[fill=black!50] (1.5,-1) rectangle +(4,2);
          \draw[thick] (1.6,1.1) -- (1.4,1.1) -- (1.4,-1.1) -- (1.6,-1.1);
          \draw[thick] (5.4,1.1) -- (5.6,1.1) -- (5.6,-1.1) -- (5.4,-1.1);
          \draw[fill=black!20] (1.5,-0.3) rectangle +(1.3,1.3);
          }
          \node (J) at (2.15,-2.5) {$J \quad=\quad \abs{\begin{array}{ccc}
                \pder{T_1}{x_1} & \cdots & \pder{T_1}{x_r}\\
                \vdots & \ddots & \vdots \\
                \pder{T_r}{x_1} & \cdots & \pder{T_r}{x_r}
              \end{array}}$};
          \only<5>{\draw[stealth-, very thick] (J) -- (2.15,-0.3);}

          \only<8->{
            \node[right of=J, node distance=6cm] (Jexp) {$ =\quad \inparen{\prod f_{ij}}\cdot \alert<10>{\abs{\begin{array}{ccc}
                \alert<9>{\pder{T_1'}{x_1}} & \cdots & \pder{T_1'}{x_r}\\
                \vdots & \ddots & \vdots \\
                \pder{T_r'}{x_1} & \cdots & \pder{T_r'}{x_r}
              \end{array}}}$};
          \only<9>{\node[below of=Jexp,node distance=1.3cm] {\Red{($s^{rk}$ sparse)}};}
          \only<10>{\node[below of=Jexp,node distance=1.3cm] {\Red{($r! s^{r^2 k}$ sparse)}};}
          \only<11>{\node[below of=Jexp,node distance=1.3cm] {product of sparse polynomials};}
          }
          
        \end{tikzpicture}
        \only<7>{
        \begin{observation}
          At most $rk$ of the $f_{ij}$'s depend on $x_1,\cdots, x_r$. 
        \end{observation}
      } 

      \only<11->{
        \vskip 20pt

        Every jacobian minor is a product of ``sparse''
        polynomials. Preserving non-zeroness is not hard.  \qed} }
\end{frame}

\begin{frame}
\frametitle{Open Problems}

\vskip 30pt

\begin{block}{PIT for bounded fan-in depth-4}
Can the Jacobian be used to construct PIT for even fan-in $2$ depth-4 circuits?
\end{block}

\vskip 30pt

\begin{block}{Jacobian and reconstruction}
  Can the Jacobian be used for \emph{reconstruction} of restricted
  arithmetic circuits?
\end{block}



\end{frame}



%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "defense"
%%% End: 
