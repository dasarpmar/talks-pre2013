(TeX-add-style-hook "chasm_at_3"
 (lambda ()
    (TeX-add-symbols
     '("tikzmark" 1)
     '("createconcept" 1)
     '("myignore" 1)
     '("Gray" 1)
     '("Code" 1)
     '("White" 1)
     '("Black" 1)
     '("Green" 1)
     '("Blue" 1)
     '("Navy" 1)
     '("Red" 1)
     '("Cyan" 1)
     '("Peach" 1)
     '("TealBlue" 1)
     "Perm"
     "Det")
    (TeX-run-style-hooks
     "multirow"
     "xltxtra"
     "fontspec"
     "textcomp"
     "fontenc"
     "T1"
     "mathdesign"
     "urw-garamond"
     "calrsfs"
     "ulem"
     "tikz"
     "latex2e"
     "beamer10"
     "beamer"
     "xetex"
     "t"
     "dvipsnames"
     "preamble")))

