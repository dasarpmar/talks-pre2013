(TeX-add-style-hook "depth4"
 (lambda ()
    (TeX-add-symbols
     '("tikzmark" 1)
     '("createconcept" 1)
     '("myignore" 1)
     '("pder" 2)
     '("parderiv" 2)
     '("Gray" 1)
     '("Code" 1)
     '("Math" 1)
     '("Emph" 1)
     '("White" 1)
     '("Black" 1)
     '("Green" 1)
     '("Navy" 1)
     '("Blue" 1)
     '("Red" 1)
     '("Cyan" 1)
     '("Peach" 1)
     '("TealBlue" 1)
     "Det"
     "Perm")
    (TeX-run-style-hooks
     "multirow"
     "xltxtra"
     "fontspec"
     "textcomp"
     "fontenc"
     "T1"
     "mathdesign"
     "urw-garamond"
     "calrsfs"
     "ulem"
     "tikz"
     "latex2e"
     "beamer10"
     "beamer"
     "xetex"
     "t"
     "dvipsnames"
     "preamble"
     "secIntro"
     "secKnownResults"
     "secSurprises"
     "secAperitif"
     "secApproachingChasm"
     "secFollowup"
     "secConclusion")))

