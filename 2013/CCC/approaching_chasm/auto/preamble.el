(TeX-add-style-hook "preamble"
 (lambda ()
    (LaTeX-add-environments
     '("proofof" 1)
     "conjecture"
     "observation"
     "proposition"
     "proof")
    (TeX-add-symbols
     '("outerprod" 2)
     '("braket" 2)
     '("ket" 1)
     '("bra" 1)
     '("zof" 2)
     '("sas" 4)
     '("floor" 1)
     '("ceil" 1)
     '("bea" 1)
     '("pderiv" 2)
     '("inv" 1)
     '("setdef" 2)
     '("norm" 1)
     '("abs" 1)
     '("inangle" 1)
     '("insquare" 1)
     '("inbrace" 1)
     '("inparen" 1)
     "eqdef"
     "union"
     "Union"
     "intersection"
     "Intersection"
     "iso"
     "zo"
     "F"
     "N"
     "Q"
     "Z"
     "C"
     "vecx"
     "vecy")
    (TeX-run-style-hooks
     "algorithm"
     "cite"
     "complexity"
     "algorithmic"
     "hyperref"
     "graphicx"
     "amsthm"
     "latexsym")))

