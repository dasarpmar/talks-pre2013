\begin{frame}
\frametitle{\only<1-4>{Chasm}\only<5>{\Red{Elevator}} at depth-4}

\begin{theorem}[\rm\Blue{[Agrawal-Vinay-08, Koiran-12, S\'{e}bastien-13]}]

\begin{center}
\begin{tikzpicture}
\node (t1) at (-3,1) {$\Perm_n$ cannot be computed by};
\node (t2) at (-3,0) {\only<1,2>{$\Sigma\Pi\Sigma\Pi$}\only<3->{\alert<3>{$\Sigma\Pi^{[\sqrt{n}]}\Sigma\Pi^{[\sqrt{n}]}$}} circuits};
\node (t3) at (-3,-1) {of \alert<4,5>{$n^{O(\sqrt{n})}$} size};

\node (t4) at (3,1) {$\Perm_n$ cannot be computed by};
\node (t5) at (3,0) {arithmetic circuits};
\node (t6) at (3,-1) {of $\poly(n)$ size};

\draw[->,thick] (-1,0) -- (1,0);
\end{tikzpicture}
\end{center}
\end{theorem}

\only<2>{
  $$
  C \quad=\quad \sum_{i=1}^s Q_{i1}\dots Q_{it}
  $$
}
\only<3>{
  $$
  C \quad=\quad \sum_{i=1}^s Q_{i1}\dots Q_{i\sqrt{n}}\quad\text{ and $\deg(Q_{ij}) = \sqrt{n}$.}
  $$
}
\only<4->{
\begin{theorem}[\rm\Red{[Gupta-Kamath-Kayal-S]}]
\begin{center}
\begin{tikzpicture}
\node (t1) at (-3,1) {$\Perm_n$ (or $\Det_n$) requires};
\node (t2) at (-3,0) {$\Sigma\Pi^{[\sqrt{n}]}\Sigma\Pi^{[\sqrt{n}]}$ circuits};
\node (t3) at (-3,-1) {of \alert<4,5>{$2^{\Omega(\sqrt{n})}$} size};
\end{tikzpicture}
\end{center}
\end{theorem}
}

\end{frame}


\begin{frame}
\frametitle{``Natural'' lower bound proofs}

\vskip 20pt

Construct a map $\Gamma:\F[x_1,\dots, x_n] \rightarrow \N$, that
assigns a number to every polynomial such that:

\onslide<2->{\Gray{Usually sub-additive: $\Gamma(f_1 + f_2) \quad \leq
    \quad \Gamma(f_1) \; + \; \Gamma(f_2)$}}

\vskip 10pt

\begin{enumerate}
\item If $f$ is computable by ``small'' circuits, then $\Gamma(f)$ is
  ``small''.

  \onslide<3->{\Gray{Typically done by showing $f$ can be written as a
      ``small'' sum \emph{building blocks}, and that each
      \emph{building block} have small $\Gamma$.}}

\vskip 10pt
\item For the desired polynomial $f$ we wish to show a lower bound, then $\Gamma(f)$ is ``large''. 

  \onslide<4->{\Gray{Hence, should require {\bf lots} of building blocks to compute it.}}

\end{enumerate}

\end{frame}

\begin{frame}
\frametitle{Weakness of building blocks}
\only<1>{
  $$
  C \quad = \quad Q_{11}\dots Q_{1d} \;+\; \dots \;+\; Q_{s1}\dots Q_{sd}
  $$
}

\only<2->{
$$
T \quad=\quad Q_1\dots Q_{d}
$$
}
\vskip 10pt
\only<3->{
{\bf Observation:} Any common root of $Q_1=\dots = Q_{d} = 0$ is a root of $T$ of ``large'' multiplicity.
}
\vskip 10pt
\only<4->{

\vskip 20pt
\begin{tikzpicture}[scale=0.9]
\only<4,5>{\node (T) at (0,0) {$\partial^{=k}(Q_1 \dots Q_{d}) \quad= $};}
\only<6->{\node (T) at (0,0) {Many solutions to:\;};}
\node (p1) at (5.5,1.5) {$(\partial Q_1) \dots (\partial Q_k) Q_{k+1} \dots Q_d \only<6>{\Red{\cdot A}}$};
\node (p2) at (5.5,-1.5) {$Q_1 \dots Q_{d-k} (\partial Q_{d-k+1}) \dots (\partial Q_d) \only<6>{\Red{\cdot B}}$};
\only<4,5>{\node (plus1) at (5.5,0.75) {$+$};
\node (plus2) at (5.5,-0.75) {$+$};
\node (dots) at (5.5,0.1) {$\vdots$};
}
\only<6>{
  \node (plus) at (5.5,0) {$\Red{+}$};
  \node (eqz) at (9.5,0) {$\Red{= 0}$};
}
  \only<5>{\node (t1) at (9.5,0) {\Gray{Large gcd}}
    edge[->,draw=gray,thick] (p1)
    edge[->,draw=gray,thick] (p2);}
\end{tikzpicture}
}
\end{frame}


\begin{frame}
\frametitle{The Complexity Measure}

\begin{eqnarray*}
  \mathbf{x}^{\leq \ell} & \stackrel{\text{def}}{=}& \text{Set of monomials of degree bounded by $\ell$}\\
\partial^{=k}(f) & \stackrel{\text{def}}{=} & \text{Set of $k$-th order partial derivatives of $f$}\\
 & & \\
 & & \\
\onslide<2->{\Gamma_{k,\ell}(f) & \stackrel{\text{def}}{=} & \dim\inbrace{\mathbf{x}^{\leq \ell} \cdot \partial^{=k}(f)}\\
&  & {\footnotesize \Gray{\text{dimension of low-degree combinations of $\partial^{=k}(f)$}}}\\
}
\end{eqnarray*}

\onslide<3->{
\begin{block}{Fact from Algebraic Geometry}
  Growth of $\Gamma_{k,\ell}(f)$ describes the geometry of roots of
  $f$ with multiplicity $k$. \Gray{(Hilbert polynomial)}
\end{block}
}

\end{frame}


\begin{frame}
  \frametitle{Upper bound for $\Sigma\Pi^{[\sqrt{n}]}\Sigma\Pi^{[\sqrt{n}]}$ circuits}
  
  
  \begin{eqnarray*}
    C & = & \sum_{i=1}^s Q_{i1}\dots Q_{i\sqrt{n}}\quad\quad \deg(Q_{ij}) = \sqrt{n}\\
    \Gamma_{k,\ell}(C) & = & \dim \inbrace{\mathbf{x}^{\leq \ell}\partial^{=k}(C)}\onslide<2->{\\
      &  &\\
      & \leq & s \cdot \dim \inbrace{\mathbf{x}^{\leq \ell}\partial^{=k}(Q_1\dots Q_{\sqrt{n}})}}\onslide<7->{\\
      & & \\
      & \leq & s \cdot \only<7>{\Blue{\binom{\sqrt{n}}{k}}\cdot \Green{\binom{n^2 + \ell + k(\sqrt{n}-1)}{n^2}}}\only<8>{\fbox{Expression 1}}}
  \end{eqnarray*}
  \only<3-7>{
  \begin{eqnarray*}
    \only<3-5>{\partial^{=k}(T)}\only<6>{\Red{\mathbf{x}^{\leq \ell}{\partial^{=k}(T)}}}\only<7->{{\mathbf{x}^{\leq \ell}\partial^{=k}(T)}} &\in & \F\text{-span}\setdef{\inparen{\prod_{i\notin S} Q_i} \only<3>{\partial^{=k}\inparen{\prod_{i\in S} Q_i}}\only<4>{\Red{\partial^{=k}\inparen{\prod_{i\in S} Q_i}}}\only<5->{\cdot \only<5,6>{\mathbf{x}^{\leq (\only<6>{\Red{\ell + }}\only<7->{\ell +}\sqrt{n}k - k)}} \only<7->{\Green{\mathbf{x}^{\leq (\ell + \sqrt{n}k - k)}}}   }   }{ \only<3-6>{\begin{array}{c}S\subseteq [\sqrt{n}]\\|S| = k \end{array}} \only<7->{\Blue{\begin{array}{c}S\subseteq [\sqrt{n}]\\|S| = k \end{array}} }} \\
  \end{eqnarray*}
}
\only<4>{
  \begin{tikzpicture}[remember picture,overlay]
    \node[xshift=1.3cm,yshift=-6.4cm, anchor=center] at (current page.north) {\Red{Degree at most $(\sqrt{n}k - k)$}};
  \end{tikzpicture}
}

\end{frame}

\begin{frame}
  \frametitle{Lower bound for $\Gamma_{k,\ell}(\Perm_n)$}

  \begin{eqnarray*}
    \only<1->{
      \Gamma_{k,\ell}(\Perm_n) & = & \dim \inbrace{\mathbf{x}^{\leq \ell}\partial^{=k}\Perm_n}\\
      & \geq & \# \inbrace{\begin{array}{l} \only<1-9>{\text{distinct leading monomials}\\\text{in }\quad\mathbf{x}^{\leq\ell} \cdot \partial^{=k}(\Perm_n)}\only<10->{\text{\alert<10>{monomials of degree $\ell + n -k$}}\\\text{\alert<10>{having an \;$(n-k)$ \emph{inc. sequence}}} } \end{array}}}
  \end{eqnarray*}
  
\only<2-10>{

\begin{center}
\begin{tikzpicture}
\draw[step=0.5] (0,0) grid (5,5);

\only<3>{
\draw[draw=red,very thick] (1,0.5) rectangle +(0.5,0.5);}

\only<4->{
\filldraw[draw=none, fill=white] (-0.02,0.98) rectangle (5.02,0.52);
\filldraw[draw=none, fill=white] (1.02,-0.02) rectangle (1.48,5.02);
}

\only<5>{
\draw[draw=red,very thick] (1.5,3) rectangle +(0.5,0.5);}

\only<6->{
\filldraw[draw=none, fill=white] (-0.02,3.01) rectangle (5.02,3.49);
\filldraw[draw=none, fill=white] (1.4,-0.02) rectangle (1.99,5.02);
}


\only<7>{
\draw[draw=red,very thick] (3.5,2) rectangle +(0.5,0.5);}

\only<8->{
\filldraw[draw=none, fill=white] (-0.02,2.01) rectangle (5.02,2.49);
\filldraw[draw=none, fill=white] (3.51,-0.02) rectangle (3.99,5.02);
}

\only<9->{
\draw[fill=red,very thick] (0,4.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (0.5,4) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (2,3.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (2.5,2.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (3,1.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (4,1) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (4.5,0) rectangle +(0.5,0.5);

}

\end{tikzpicture}

\end{center}
}
\end{frame}

\begin{frame}
\frametitle{Counting...}


\begin{center}
\begin{tikzpicture}[scale=0.8]
\draw[step=0.5] (0,0) grid (5,5);

\only<2,3>{
\draw[fill=green] (0,2.5) rectangle +(0.5,0.5);
\draw[fill=green] (0,4) rectangle +(0.5,0.5);
\draw[fill=green] (0,4) rectangle +(0.5,0.5);
\draw[fill=green] (0,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (0,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (0,3) rectangle +(0.5,0.5);
\draw[fill=green] (0,0) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,4.5) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,2) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,0) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,0) rectangle +(0.5,0.5);
\draw[fill=green] (0.5,3) rectangle +(0.5,0.5);
\draw[fill=green] (1,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (1,1) rectangle +(0.5,0.5);
\draw[fill=green] (1,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (1,2.5) rectangle +(0.5,0.5);
\draw[fill=green] (1,2) rectangle +(0.5,0.5);
\draw[fill=green] (1,1.5) rectangle +(0.5,0.5);
\draw[fill=green] (1,4) rectangle +(0.5,0.5);
\draw[fill=green] (1.5,0) rectangle +(0.5,0.5);
\draw[fill=green] (1.5,2.5) rectangle +(0.5,0.5);
\draw[fill=green] (1.5,1.5) rectangle +(0.5,0.5);
\draw[fill=green] (1.5,4.5) rectangle +(0.5,0.5);
\draw[fill=green] (1.5,4.5) rectangle +(0.5,0.5);
\draw[fill=green] (1.5,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (2,2.5) rectangle +(0.5,0.5);
\draw[fill=green] (2,4) rectangle +(0.5,0.5);
\draw[fill=green] (2,2.5) rectangle +(0.5,0.5);
\draw[fill=green] (2,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (2,1) rectangle +(0.5,0.5);
\draw[fill=green] (2,0) rectangle +(0.5,0.5);
\draw[fill=green] (2.5,0) rectangle +(0.5,0.5);
\draw[fill=green] (2.5,2) rectangle +(0.5,0.5);
\draw[fill=green] (2.5,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (3,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (3,2) rectangle +(0.5,0.5);
\draw[fill=green] (3,0) rectangle +(0.5,0.5);
\draw[fill=green] (3,3) rectangle +(0.5,0.5);
\draw[fill=green] (3,3) rectangle +(0.5,0.5);
\draw[fill=green] (3,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,4.5) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,3) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,1.5) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,3) rectangle +(0.5,0.5);
\draw[fill=green] (3.5,3) rectangle +(0.5,0.5);
\draw[fill=green] (4,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (4,0) rectangle +(0.5,0.5);
\draw[fill=green] (4,0.5) rectangle +(0.5,0.5);
\draw[fill=green] (4,0) rectangle +(0.5,0.5);
\draw[fill=green] (4,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (4,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (4,1.5) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,2) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,3.5) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,1.5) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,1) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,1) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,1.5) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,3) rectangle +(0.5,0.5);
\draw[fill=green] (4.5,4) rectangle +(0.5,0.5);
\draw[fill=green] (4,4.5) rectangle +(0.5,0.5);
}

\draw[fill=red,very thick] (0,4.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (0.5,4) rectangle +(0.5,0.5);
\only<1,2,4->{\draw[fill=red,very thick] (2,3.5) rectangle +(0.5,0.5);}
\draw[fill=red,very thick] (2.5,2.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (3,1.5) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (4,1) rectangle +(0.5,0.5);
\draw[fill=red,very thick] (4.5,0) rectangle +(0.5,0.5);



\only<3>{
  \draw[fill=red!, ultra thick] (1,3.5) rectangle +(0.5,0.5);
  \draw[fill=green, ultra thick, dashed] (2,3.5) rectangle +(0.5, 0.5);
}

\only<5->{
  \draw[fill=black!50, thick] (1,3.5) rectangle +(0.5,0.5);
  \draw[ultra thick] (1,4) -- +(0.5,-0.5);
  \draw[ultra thick] (1,3.5) -- +(0.5,0.5);

  \draw[fill=black!50, thick] (1.5,3.5) rectangle +(0.5,0.5);
  \draw[ultra thick] (1.5,4) -- +(0.5,-0.5);
  \draw[ultra thick] (1.5,3.5) -- +(0.5,0.5);

  \draw[fill=black!50, thick] (2.5,3) rectangle +(0.5,0.5);
  \draw[ultra thick] (2.5,3.5) -- +(0.5,-0.5);
  \draw[ultra thick] (2.5,3) -- +(0.5,0.5);


  \draw[fill=black!50, thick] (3,3) rectangle +(0.5,0.5);
  \draw[ultra thick] (3,3.5) -- +(0.5,-0.5);
  \draw[ultra thick] (3,3) -- +(0.5,0.5);

  \draw[fill=black!50, thick] (3,2) rectangle +(0.5,0.5);
  \draw[ultra thick] (3,2.5) -- +(0.5,-0.5);
  \draw[ultra thick] (3,2) -- +(0.5,0.5);

  \draw[fill=black!50, thick] (3.5,2) rectangle +(0.5,0.5);
  \draw[ultra thick] (3.5,2.5) -- +(0.5,-0.5);
  \draw[ultra thick] (3.5,2) -- +(0.5,0.5);


  \draw[fill=black!50, thick] (3.5,1) rectangle +(0.5,0.5);
  \draw[ultra thick] (3.5,1.5) -- +(0.5,-0.5);
  \draw[ultra thick] (3.5,1) -- +(0.5,0.5);

  \draw[fill=black!50, thick] (4.5,0.5) rectangle +(0.5,0.5);
  \draw[ultra thick] (4.5,1) -- +(0.5,-0.5);
  \draw[ultra thick] (4.5,0.5) -- +(0.5,0.5);
}
\end{tikzpicture}
\end{center}
\only<6->{
\Blue{``Most''} increasing sequences have about \Green{$2k$} forbidden elements. 
}

\only<7->{
 $$
 \Gamma_{k,\ell}(\Perm_n) \quad\geq \quad \only<7>{\Blue{\binom{n+k}{2k}} \cdot \binom{\ell + \Green{n^2 - 2k}}{\Green{n^2 - 2k}}}\only<8>{\fbox{Expression 2}}
 $$
}

\end{frame}


\begin{frame}
\frametitle{Putting it together}

  \begin{eqnarray*}
    C & = & \sum_{i=1}^s Q_{i1}\dots Q_{i\sqrt{n}}\quad\quad \deg(Q_{ij}) = \sqrt{n}
  \end{eqnarray*}
  
  \pause
  \begin{eqnarray*}
    \Gamma_{k,\ell}(C) & \leq & s \cdot \fbox{Expression 1}\\\pause
    \Gamma_{k,\ell}(\Perm_n) & \geq & \fbox{Expression 2}\\
  \end{eqnarray*}
  \pause
  \vskip 10pt

  \quad \quad Choosing $\ell$ and $k$ carefully, \quad\quad$s = 2^{\Omega(\sqrt{n})}$.
  


\end{frame}

\begin{frame}
\frametitle{Determinant vs Permanent}

\begin{eqnarray*}
  \Gamma_{k,\ell}(\Perm_n) & = & \dim \inbrace{\mathbf{x}^{\leq \ell}\partial^{=k}\Perm_n}\\
  & \Red{>} & \# \inbrace{\begin{array}{l} \text{monomials of degree $\ell + n -k$}\\\text{having an \;$(n-k)$ \emph{inc. sequence}} \end{array}}\\
  & &\\
  &\onslide<3->{\begin{tikzpicture}
    \node at (0,0) {\rotatebox{-90}{\Red{\Huge >}}};
  \end{tikzpicture}}
  & \onslide<4>{\quad\quad\Red{\fbox{Question: But how much larger?}}}\\
  & &\\\onslide<2->{
  \Gamma_{k,\ell}(\Det_n) & = & \dim \inbrace{\mathbf{x}^{\leq \ell}\partial^{=k}\Det_n}\\
  & \Red{=} & \# \inbrace{\begin{array}{l} \text{monomials of degree $\ell + n -k$}\\\text{having an \;$(n-k)$ \emph{inc. sequence}} \end{array}}\\
  & & \text{\Blue{[Sturmfels90]}: $\Det_n$ minors form a gr\"{o}bner basis}
}
\end{eqnarray*}
\end{frame}

\begin{frame}
\frametitle{What next?}

\pause

\begin{block}{Limitation of this method}
  Best lower bound this measure can give is $n^{\Omega(\sqrt{d})}$.
\end{block}

\pause 
\vskip 20pt

{\bf Question: }Can we achieve an $n^{\Omega(\sqrt{d})}$? \pause \Red{Yes!}

\vskip 20pt

\begin{theorem}[\Red{[Kayal-Saha-S]}]
  There is an explicit polynomial in $\VNP$ that requires
  $\Sigma\Pi^{[\sqrt{d}]}\Sigma\Pi^{[\sqrt{d}]}$ circuits of top
  fan-in $n^{\Omega(\sqrt{d})}$.\pause

  Also gives super-polynomial \Emph{regular formula} lower bounds.
\end{theorem}

\pause 

\vskip 20pt

\Gray{Any asymptotic improvement implies $\VP \neq \VNP$!}
\end{frame}

\begin{frame}

\vskip 65pt

\begin{conjecture}
$\VP$ vs $\VNP$ would be resolved within the next 5 years. 
\end{conjecture}

\vskip 20pt

Let's get started...

\vskip 50pt

\hfill {\fontsize{35}{40} \Navy{\rm  Thank you!}}

\hfill {\fontsize{25}{30} \Navy{\rm  Questions?}}
\end{frame}




%%% Local Variables: 
%%% mode: latex
%%% TeX-master: "CCC"
%%% End: 
