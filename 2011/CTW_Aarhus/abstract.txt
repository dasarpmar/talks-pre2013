Title: Jacobian hits all circuits

Abstract:

We present a unified approach to polynomial identity testing that
generalizes all known black-box PITs for constant depth formulae over
characteristic zero fields. In particular, we give generalizations of
the [Kayal-Saxena] bounded fan-in depth 3 PIT, and generalizations of
PITs for read-k bounded depth formulae [Saraf-Volkovich,
Anderson-vanMelkebeek-Volkovich]. The approach for all of these
results is via a powerful tool called the Jacobian.

This is ongoing work with Manindra Agrawal, Chandan Saha and Nitin Saxena. 
