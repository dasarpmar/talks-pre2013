\documentclass{beamer}
\mode<presentation>
{
\usetheme{Boadilla}
}
\input{preamble}
\renewcommand{\R}{\mathcal{R}}
\renewcommand{\F}{\mathcal{F}}
\title[Integer Multiplication]{Fast Integer Multiplication Using Modular Computation}
\author[Ramprasad Saptharishi]{Ramprasad Saptharishi\\ (joint work with
  Anindya De, Piyush P Kurur, Chandan Saha)
}
\institute[CMI \& IITK]{
  Chennai Mathematical Institute\\
  and\\
  Indian Institute of Technology, Kanpur\\
  Ph.D Student
}
\date

\AtBeginSection[]{
\begin{frame}<beamer> 
\frametitle{}
\tableofcontents[currentsection]
\end{frame}
}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}
\frametitle{A Different Answer to the Guard}

\begin{block}{}
Guard: ``Saar, what are you people doing?''

Vinay: ``Doing some mathematics problem.''

Guard: ``Four people together? Are you guys multiplying some huge numbers?''
\end{block}
\bigskip
\pause

{\bf Problem Statement: } Given two $N$ bit integers, multiply them. 
\end{frame}

\section{Some Earlier Approaches}

\begin{frame}[fragile]
\frametitle{The School Method}
\begin{verbatim}
   234
x  129
------
  2106
  468
 234 
------
 30186
\end{verbatim} 
Time Complexity: $O(N^2)$ 
\end{frame}

\begin{frame}
\frametitle{Karatsuba-Offman '63}
\begin{block}{}
\begin{eqnarray*}
(A\cdot2^i + B)(C\cdot2^i + D) & = & AC\cdot2^{2i} + (AD + BC)\cdot2^i + BD\\
  & = & AC\cdot2^{2i} + BD\\
  &   & + \inparen{(A+B)(C+D) - AC - BD}\cdot2^i
\end{eqnarray*}
\end{block}
Time Complexity: $O(N^{\log_2 3})$
\end{frame}

\begin{frame}
\frametitle{Toom '63}
\begin{theorem}
  For every constant $\varepsilon > 0$, there is an algorithm to
  multiply two $N$-bit integers in time $O(N^{1+\varepsilon})$.
\end{theorem}
\end{frame}

\section{Sch\"{o}nhage-Strassen's Breakthrough}

\begin{frame}
\frametitle{Enter Polynomials}
\begin{itemize}
\item Convert the bit representation of the integer into a polynomial
  over a suitable ring. For eg, $\sum a_i2^i \mapsto \sum a_ix^i$. 
\item Multiply the polynomials
\item Evaluate the product polynomial at an appropriate place. 
\end{itemize}
\pause
How fast can we multiply polynomials?
\end{frame}

\begin{frame}
\frametitle{Polynomial Addition vs Polynomial Multiplication}

{\bf Question:} Why is addition easy while multiplication seems to
take more time?\pause

{\bf Answer(?):} Addition is pointwise addition on the
coefficients. Multiplication isn't.\pause

What about other representations? Can we find some representation
where multiplication is component-wise as well?\pause \emph{Yes;
  evaluations at some chosen points}.
\end{frame}

\begin{frame}
\frametitle{Evaluation and Interpolation}

\begin{observation}
Given $N$ points $\inbrace{(x_i,y_i)}$, there is a unique polynomial $p(x)$
of degree less than $N$ such that $p(x_i) = y_i$ for all $i$. 
\end{observation}
\pause
Hence, if we represent polynomials with just their values at some
chosen points, then both multiplication and addition is point-wise. 
\pause

\medskip
If the chosen points are the $N$-th roots of unity, then this
representation is called the \emph{Fourier Representation}. \pause

\medskip
Given a polynomial as a list of coefficients, how quickly can we
compute its values at roots of unity?

\end{frame}

\begin{frame}
\frametitle{The Fast Fourier Transform}

Let $\omega$ be the $N$-th root of unity we are using for the fourier
transform. Let $f(x) = (x^{N/2}  - 1)q(x) + f_0(x)$ and let $f(x) =
(x^{N/2} + 1)q'(x) + f_1(x)$. \pause
\begin{itemize}
\item For even powers of $\omega$, $f(\omega^{2i}) =
  f_0(\omega^{2i})$.\pause
\item For odd powers of $\omega$, $f(\omega^{2i+1}) =
  f_1(\omega^{2i+1})$. \pause Suppose $g(x) = f(\omega x)$, then
  $f(\omega^{2i+1}) = g(\omega^{2i})$. 
\end{itemize}
\pause
Since $\omega^2$ is a $N/2$-th root of unity, this is just taking a
fourier transform of $f_0(x)$ and $f_1(\omega x)$. \pause

\medskip
Time Complexity: $O(N\log N)$ operations on the base ring.

\end{frame}

\begin{frame}
\frametitle{Sch\"{o}nhage-Strassen '71 - Algo $1$}

\begin{itemize}
\item Convert the $N$-bit integers into two univariate polynomials of
  degree at most $M = \inparen{\frac{N}{\log^2 N}}$ with the
    coefficients being $O(\log^2 N)$ bit long.\pause
\item Multipying these two polynomials requires an $2M$-th root of
  unity. \pause Well, work over $\C$ and use $e^{2\pi i/2M}$ as the
  root of unity. \pause
\item But $\C$ is infinite! \pause With quite a bit of sweat and blood,
  show that keeping a small precision would be sufficient. \pause
\item Compute the FFT, multiply component-wise, take the inverse FFT. 
\end{itemize}

\pause
Time Complexity:
\begin{eqnarray*}
T(N) & = & O\inparen{M\log M T(O(\log^2N))}\\
     & = & O\inparen{\frac{N}{\log N}T(O(\log^2N))}\\
     & = & O(N\cdot\log N\cdot\log\log N\cdots 2^{O(\log^*N)})
\end{eqnarray*}

\end{frame}


\begin{frame}
\frametitle{Sch\"{o}nhage-Strassen '71 - Algo $2$}

\begin{observation}
The FFT only involved multiplication by \emph{roots of unity}, not
arbitrary elements of the base ring. 
\end{observation}

We need a ``good'' $2M$-th root of unity to do the FFT. \pause
  ``Manufacture one!''. Work over the ring
$$
\R = \frac{\Z}{(2^M + 1)\Z}
$$
$2$ is a root of unity, which is easy to multiply (just shifts). They
choose $M = \sqrt{N}$.


\pause
Time Complexity:
$$
T(N) = O\inparen{\sqrt{N}\log N + \sqrt{N}T(\sqrt{N})} = O(N\log
N\log\log N)
$$

\end{frame}

\begin{frame}
\frametitle{Pros and Cons}
$$
\R = \C
$$
\pause
\begin{itemize}
\item Good News: The ring size was small (quasipolynomial in $N$, once
  we consider finite precision). \pause Therefore, we could reduce
  multiplication of $N$ bit integers to multiplication of $O(\log^2
  N)$ bit integers. \pause
\item Bad News: The root of unity is ``bad''.
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Pros and Cons}
$$
\R = \frac{\Z}{2^{\sqrt{N}} + 1}
$$
\pause
\begin{itemize}
\item Good News: The ring has $2$ as a root of unity. \pause
  Therefore, the fourier transform just took time $O(\sqrt{N}\log
  N)$. If we had some arbitrary root of unity, multiplications would
  cost us well, and we could have got $O(\sqrt{N}\log
  N)T(\sqrt{N})$. \pause
\item Bad News: The size of the ring is $O(2^{\sqrt{N}})$, which is
  exponential in the order of the root we want. \pause As a result,
  this is the best we can do in this setting. \pause
\end{itemize}
[SS71] was the best for more than $35$ years \pause until F\"{u}rer.
\end{frame}

\section{F\"{u}rer's Algorithm}

\begin{frame}
\frametitle{The Best of Both Worlds}
\begin{itemize}
\item We want the ring size to be small (polynomial or quasipolynomial
  $N$).\pause
\item The root of unity must be ``good''. This is to make sure that
  the fourier transforms aren't too costly. \pause
\end{itemize}
Finding both together would be ideal, but is perhaps too much to ask
for. \pause

\medskip
How about a compromise that maybe multiplication by the root of unity
is bad, but maybe some of its powers are easy to multiply? Then not
all the multiplications would cost too much. \pause
$$
\R = \frac{\C[\alpha]}{\alpha^m + 1}\quad,m = O(\log N)
$$
With finite precision of course!
\end{frame}
\begin{frame}
\frametitle{The Root of Unity}
$$
\R = \frac{\C[\alpha]}{\alpha^m + 1}
$$
We want an $2M$-th root of unity $\rho(\alpha)\in \R$ such that some
power of $\rho(\alpha) = \alpha$. Let $\omega$ be a $2M$-th root of
unity in $\C$ and $\sigma = \omega^{M/m}$ be a $2m$-th root of unity in $\C$. Choose
$\rho(\alpha)$ so that
\begin{eqnarray*}
  \rho(\sigma^{2i+1}) & = & \omega^{2i+1}\quad 1\leq i \leq m\\\pause
  \implies   \inparen{\rho(\sigma^{2i+1})}^{M/m} & = &
  \omega^{(2i+1)M/m} = \sigma^{2i+1}\\\pause
  \implies \inparen{\rho(\alpha)}^{M/m} & = & \alpha \pmod{\alpha -
    \sigma^{2i+1}}\quad 1\leq i \leq m\\\pause
  \implies \inparen{\rho(\alpha)}^{M/m} & = & \alpha \pmod{ \alpha^m +
    1}\pause
\end{eqnarray*}
$$
\rho(\alpha) = \sum_{i=1}^{m}\omega^{2i+1}\frac{\prod_{j\neq i}(\alpha
  - \sigma^{2j+1})}{\prod_{j\neq i}(\sigma^{2i+1} - \sigma^{2j+1})}
$$
\end{frame}

\begin{frame}
\frametitle{The Root of Unity}

\begin{itemize}
\item The integers mapped into polynomials over $\R$ 
  $$
  a \mapsto \sum_{i=0}^{M-1} r_i x^i\quad,\quad r_i \in \R =
  \frac{\C[\alpha]}{\alpha^m + 1}
  $$
\item The parameter $M = O\inparen{\frac{N}{\log^2N}}$. 
\item Multiplying any element of $\R$ by $\alpha$ is just a
  shift. Therefore, some multiplications by some powers of
  $\rho(\alpha)$ are efficient.

\item Use a slightly modified FFT with this root $\rho(\alpha)$. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Size of the Ring}

\begin{itemize}
\item We can't obviously work with $\C$. \pause
\item Work with complex numbers with some finite precision. \pause
\item As long as we use enough precision, we can still recover the
  integer product. \pause
\item Again, with considerable effort, one can show that a precision
  of $O(\log^2N)$ would be sufficient. \pause
\end{itemize}

{\bf Observation: }The size of the ring is essentially only
$2^{O(\log^2 N)}$. 
\end{frame}

\begin{frame}
\frametitle{The Fast(er) Fourier Transform (sketch)}

\begin{itemize}
\item Recall that the traditional FFT was done by just essentially
  working with $\inbrace{\omega^{2i}}_i$, the powers of the $\omega^2$.
\item This can be generalized to any other powers as well. \pause If
  $N = AB$,
$$
\F_N = B\F_A + A\F_B + AB(\text{multiplications by roots})
$$
\item In F\"{u}rer's setting, he works with powers of
  $\inparen{\rho(\alpha)}^{M/m}$, which are just powers of $\alpha$.
\item All the ``inner'' fourier transforms are efficient.\pause
\end{itemize}
Time Complexity: 
$$
T(N) = O\inparen{N\log N + \frac{N}{\log N\log\log N}T(O(\log^2N))}
$$
\end{frame}

\section{A Modular Algorithm}

\begin{frame}
\frametitle{Why do we want a modular algorithm?}
\pause
\begin{itemize}
\item It gives a sense of closure.\pause
\item Algorithms that involve complex numbers inevitably have an error
  analysis. This is virtually absent in the case of modular algorithms.\pause
\item A discrete problem like integer multiplication should have a
  discrete solution. 
\end{itemize}

\end{frame}

\begin{frame}
\frametitle{Difficulties in Naive Adaptation}

Let say we work with $\R =
\frac{\Z}{p\Z}\frac{[\alpha]}{\alpha^m+1}$. This contains a $(p-1)$-th
root of unity. \pause If we want to multiply two $M$-degree
polynomials, we require a $2M$-th root of unity. \pause Therefore, $2M
\mid (p-1)$ or $p\equiv 1\bmod{2M}$. \pause

How do we find such a prime?\pause

\begin{proposition}
Assuming ERH, a prime $p$ such that $p\equiv 1\pmod{2M}$ can be
computed by a randomized algorithm with expected running time
$\tilde{O}(\log^3 M)$.
\end{proposition}
\pause
Bah! This is too much! Can we do it determinstically?
\end{frame}

\begin{frame}
\frametitle{Picking the prime}

Firstly, can we always find such primes? \pause Yes, infact Dirichelet
says that there are infinitely many of them. \pause But the first of
that kind can be huge! \pause Linnik says no.

\begin{theorem}[Linnik]
There exists constants $\ell$ and $L$ such that the first prime $p$ of
the form $p\equiv 1\bmod{K}$ is less than $\ell K^L$.
\end{theorem}
\pause Great! Can we just check the first few elements of the
arithmetic progression and find a prime? \pause Not quite, $M =
\frac{N}{\log^2N}$ and the best known bound for $L$ is $5.5$
     [Heath-Brown]. \pause Can we somehow make $M$, the degree,
     smaller? \pause Yes, use multivariate polynomials!
\end{frame}

\begin{frame}
\frametitle{Using $k$-variate polynomials}
\begin{itemize}
\item If $M$ is the degree bound on each variable and there are $k$
  variables, the number of monomials is $M^k$. Choose $M^k =
  \inparen{\frac{N}{\log^2N}}$.\pause
\item Since the degree bound in each variable is just $M$, we just
  need a $2M$-th root of unity to do the FFT.\pause 
\item Choose just about enough variables so that $M^L =
  \inparen{\frac{N}{\log^2N}}^{L/k} = N^{\varepsilon}$. This makes the
  search for the prime feasible. \pause
\item Work in the ring $\R =
  \frac{\Z}{p^c\Z}\frac{[\alpha]}{\alpha^m+1}$. This $c$ is to prevent
  ``overflows''. 
\end{itemize}

Both $c$ and $k$ can be easily seen to be just constants. 
\end{frame}

\begin{frame}
\frametitle{Multivariate FFT?}\pause
\begin{itemize}
\item This is not intimidating. In fact, it is exactly the same as
  univariate FFT once you look at it in a certain perspective. \pause
\item The FFT can be seen as just a change of basis.
\item A clean group-theoretic description of the FFT can be seen in
  the paper.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Time Complexity}
\pause
Exactly the same!
\begin{eqnarray*}
T(N) & = & O\inparen{N\log N + \frac{N}{\log N\log\log
    N}T(O(\log^2N))}\\
     & = & O\inparen{N\cdot \log N\cdot 2^{O(\log^* N)}}
\end{eqnarray*}
\end{frame}

\section{Conclusions}

\begin{frame}
\frametitle{A Note on ``Errors'' in Modular Computation}
The error analysis is present in a very subtle way. \pause
\begin{itemize}
\item Working in $\Z/p^c\Z$ is just considering elements in $\Q_p$
  with ``a precision of $p^{-c}$''.\pause
\item The field $\Q_p$ has a norm $|\ \cdot\ |$ called the $p$-adic
  norm.\pause
\item This norm is non-archimidean, that is satisfies the stronger
  traingle inequality $\abs{x+y}_p \leq
  \max\inbrace{\abs{x}_p,\abs{y}_p}$. \pause As a result, errors don't
  compound!\pause
\item Thus, in a way, the modular setting and complex setting aren't
  too different after all. 
\end{itemize}
\end{frame}

\title[An ``Electric'' Story of a Drunkard]{Thank You}
\author[Ramprasad Saptharishi]{Slides are available at \texttt{$\sim$ramprasad/studenttalks/drunkard.pdf}.}
\institute{}

\title[Integer Multiplication]{Thank You}
\author[Ramprasad Saptharishi]{Questions?}
\institute[CMI \& IITK]{}
\date{}
\begin{frame}
\titlepage
\end{frame}
\end{document}
