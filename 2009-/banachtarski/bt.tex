\documentclass{beamer}
\mode<presentation>{\usetheme{Singapore}}
\input{preamble}

\title[Banach-Tarski Paradox]{The Banach-Tarski Paradox}

\author[Ramprasad Saptharishi]{Ramprasad Saptharishi}

\institute[CMI \& IITK]{
  Chennai Mathematical Institute\\
  and\\
  Indian Institute of Technology, Kanpur\\
  Ph.D Student
}

\AtBeginSection[]{
\begin{frame}<beamer> 
\frametitle{}
\tableofcontents[currentsection]
\end{frame}
}
\AtBeginSubsection[]{
\begin{frame}<beamer> 
\frametitle{}
\tableofcontents[currentsubsection]
\end{frame}
}


\begin{document}


\begin{frame}
  \titlepage
\end{frame}




\section{Introduction}


\subsection{Axiom of Choice}



\begin{frame}
\frametitle{The Statement: Informal}

Given a large number of boxes of oranges, each box being non-empty,
then one can create a box of oranges consisting of exactly one from
each box. 

\bigskip \pause

\emph{One can pick a representative of each box}
\end{frame}




\begin{frame}
\frametitle{The Statement: Formal}

\begin{block}{Axiom of Choice}
Let $\mathcal{A}$ be a collection of non-empty sets. Then, there
exists a \emph{choice function}
$$
f:\mathcal{A} \longrightarrow \Union_{A\in \mathcal{A}} A
$$
such that $f(A) \in A$ for all $A\in \mathcal{A}$.
\end{block}
\end{frame}




\begin{frame}
\frametitle{Okay... so?}
\begin{itemize}
\pause
\item It is an axiom (independent of ZF)\pause
\item Seems ``obviously'' true\pause
\item Required for various fundamental results:
  \begin{itemize}
  \item Every vector space has a basis
  \item Every field has an algebraic closure
  \item Given two sets $A$, $B$, either $|A| > |B|$ or $|B| > |A|$ or
    $|A| = |B|$
  \item Product of non-empty sets is non-empty
  \end{itemize}
\end{itemize}
\end{frame}



\begin{frame}
  \frametitle{What's the harm in using it?}
  
  Well... some consequences of AoC are extremely
  counter-intuitive, like the \emph{well-ordering theorem}. \pause The
  most bizarre (IMHO) being the Banach-Tarski Paradox. 

\end{frame}



\subsection{The Banach-Tarski Paradox}



\begin{frame}
\frametitle{The Ultimate Jigsaw Puzzle}

\begin{proposition}[Banach-Tarski] Assuming the axiom of choice, it is 
  possible to partition the solid unit sphere into $6$ pieces
  $A_1$,...,$A_6$, and just rotate/translate these pieces to
  $A_1'$,...,$A_6'$ which a partition of two disjoint unit spheres.
\end{proposition}

\pause\bigskip

{\bf Disclaimer:} I am being a mathematician. By sphere, I mean 
$$
\mathcal{B}_3 = \setdef{(x,y,z)\in \R^3}{x^2+y^2+z^2 \leq 1}
$$
\end{frame}



\begin{frame}
\frametitle{The Ultimate Jigsaw Puzzle}
\begin{itemize}
\item What about volume preservation? \pause Intermediate pieces have
  no well-defined volume (non-measurable). More like dust rather than
  pieces. Hence, volume invariance breaks down.\pause
\item We'll see a proof, not involving $6$ but certainly less than
  $20$ pieces. 
\end{itemize}
\end{frame}



\section{Appetiser to the Proof}



\subsection{Broken Circle $\equiv$ Circle}


\begin{frame}
\frametitle{The Circle and the Broken Circle}


\begin{eqnarray*}
\mathcal{S}_1 & = & \setdef{z\in \C}{|z| = 1}\\
\tilde{\mathcal{S}}_1 & = & \mathcal{S}_1 - \inbrace{1}\\
\end{eqnarray*}
\pause

Consider this partition of the circle:
\begin{eqnarray*}
A & = & \setdef{e^{in}}{n = 0,1,2,\cdots}\\
B & = & \mathcal{S}_1 - A
\end{eqnarray*}

\pause
$\rho:z\mapsto e^{i}z$, rotation by one radian counterclockwise. 
$$
A' = \rho(A) \pause= \rho\inparen{\inbrace{e^{0i},e^{1i},e^{2i},\cdots}} \pause=
\inbrace{e^{1i},e^{2i},e^{3i}\cdots} = \pause A - \inbrace{1}
$$\pause
Keep $B$ as it is and we get the broken circle. 
\end{frame}



\subsection{$F_2 \equiv F_2\union F_2$}



\begin{frame}
\frametitle{The Free Group}
\begin{block}{The Free Group over $2$ Generators}
The elements of this groups are strings over $a,a^{-1},b,b^{-1}$ where
multiplication is concatenation. And you collapse sequences like
$aa^{-1}$ to the identity of the empty string. Hence, $aabb^{-1}a^{-1}
= a$.
\end{block}
\end{frame}



\begin{frame}
\frametitle{The Group $F_2$}
\includegraphics[width=3in]{cayley_F2.png}
\end{frame}



\begin{frame}
\frametitle{The Partition}
\includegraphics[width=3in]{cayley1.png}
\end{frame}



\begin{frame}
\frametitle{One Piece}
\includegraphics[width=3in]{F2_1.png}
\end{frame}



\begin{frame}
\frametitle{After multiplication by $a$}
\includegraphics[width=3in]{F2_2.png}
\end{frame}



\begin{frame}
\frametitle{Two Copies!!!}
\includegraphics[width=3in]{cayley1.png}
\pause ... and a dot
\end{frame}



\begin{frame}
\frametitle{Slight Surgery}
\includegraphics[width=3in]{cayley2.png}
\end{frame}



\begin{frame}
\frametitle{Incredible} $F_2$ can be written as a disjoint union of
$\mathcal{A}_1$, $\mathcal{A}_2$, $\mathcal{A}_3$, $\mathcal{A}_4$ that
can be used to produce two copies of $F_2$. \pause\bigskip

Can we find a structure like $F_2$ inside the solid sphere?
\end{frame}



\section{Doubling the Sphere}



\subsection{Preliminaries}



\begin{frame}
\frametitle{The Sphere and the Shell}
\begin{eqnarray*}
\mathcal{B}_3 & = & \setdef{(x,y,z)\in \R^3}{x^2 + y^2 + z^2 \leq 1}\\
\mathcal{S}_2 & = & \setdef{(x,y,z)\in \R^3}{x^2 + y^2 + z^2 = 1}
\end{eqnarray*}
\pause
We shall just restrict ourselves to finding a \emph{paradoxical
  decomposition} in the spherical shell. Very easy to lift it to the
entire sphere. 
\end{frame}



\begin{frame}
\frametitle{The Group of Rotations}\pause
\begin{itemize}
\item The set of rotations act on the sphere; are characterized by
  the axis and the angle. \pause
\item They form a group. 
\item Every non-trivial rotation has exactly two fixed points.
\end{itemize}

\pause
Does there exist a copy of $F_2$ sitting inside this group? \pause Yes
\end{frame}



\begin{frame}
\frametitle{$F_2$ Inside the Group of Rotations}\pause
\begin{claim}
Let $\sigma$ be the rotation about the $x$-axis by $1$ radian and
$\tau$ be the rotation about the $y$ axis by $1$ radian. Then,
$\sigma$ and $\tau$ generate a free group. 
\end{claim}
\pause
\begin{proof}
... try it out. 
\end{proof}
\bigskip

\pause Great. Lets call this group $\mathcal{F}$. \pause 

Recall that $\mathcal{F}$ can be decomposed into
$\mathcal{A}_1$,$\mathcal{A}_2$,$\mathcal{A}_3$,$\mathcal{A}_4$ such
that
$$
\mathcal{F} = \mathcal{A}_1 \union \sigma \mathcal{A}_2 =
\mathcal{A}_3 \union \tau\mathcal{A}_4
$$
\end{frame}



\begin{frame}
\frametitle{The \emph{Almost}-Sphere}
Each non-trivial $\rho\in \mathcal{F}$ has precisely two fixed
points on $\mathcal{S}_2$. Let $D$ be the set of all fixed points of
$\mathcal{F}$. 
$$
\tilde{\mathcal{S}}_2 = \mathcal{S}_2 - D
$$
\end{frame}



\begin{frame}
\frametitle{Roadmap}
\begin{itemize}
\item Transform $\mathcal{S}_2$ to $\tilde{\mathcal{S}_2}$
\item Double $\tilde{\mathcal{S}_2}$, forming two copies of it
\item Transform each copy of $\tilde{\mathcal{S}_2}$ back to
  $\mathcal{S}_2$
\end{itemize}


Step $1$ and $3$ are inverses of each other; we shall cover that after
we look at step $2$. 
\end{frame}


\subsection{Doubling the almost-sphere}

\begin{frame}
\frametitle{Orbits of $\tilde{\mathcal{S}_2}$}

\begin{claim}
For any $\rho\in \mathcal{F}$, $p\in D$ if and only if $\rho(p) \in
D$.
\end{claim}
\begin{proof}
If $\sigma(p) = p$, then $\rho \sigma \rho^{-1}(\rho(p)) = \rho(p)$.
\end{proof}
\pause
Hence, the group $\mathcal{F}$ acts on $\tilde{\mathcal{S}_2}$ as
well. \pause Look at the set of orbits and pick a set $R$ of
representatives (using the \emph{Axiom of Choice}).

\end{frame}



\begin{frame}
\frametitle{The Partition}

Consider the following sets:
$$
A_i = \setdef{\rho(R)}{\rho\in\mathcal{A}_i}\qquad,\qquad
i = 1,2,3,4
$$\pause
\begin{claim}
This forms a disjoint partition of $\tilde{\mathcal{S}_2}$. 
\end{claim}\pause
\begin{proof}
If $\rho_1(r) = \rho_2(r)$ for $\rho_1\neq \rho_2$, then $r$ will be a
fixed point of $\rho_1^{-1}\rho_2$.
\end{proof}
\end{frame}



\begin{frame}
\frametitle{Doubling the \emph{almost}-sphere}
\begin{claim}$\tilde{\mathcal{S}_2} = A_1 \union \sigma(A_2) = A_3 \union \tau(A_4)$
\end{claim}
\pause
\begin{proof}\begin{eqnarray*}
  \sigma(A_2) & = & \sigma\inparen{\setdef{\rho(R)}{\rho\in \mathcal{A}_2}}\\\pause
              & = & \setdef{\sigma\rho(R)}{\rho\in \mathcal{A}_2}\\\pause
              & = & \setdef{\rho'(R)}{\rho'\in \sigma \mathcal{A}_2} \\\pause
              & = & \setdef{\rho'(R)}{\rho'\in  \mathcal{A}_2\union \mathcal{A}_3\union \mathcal{A}_4}\\
              & = & A_2 \union A_3 \union A_4\\\pause
\implies A_1 \union \sigma(A_2) & = & \tilde{\mathcal{S}_2}
\end{eqnarray*}
\end{proof}
\end{frame}



\subsection{Sphere to Almost-Sphere}


\begin{frame}
\frametitle{Finding a ``good'' rotation}

\begin{itemize}
\item $D$ is countable. \pause Pick an axis that does not pass through
  any point of $D$. 
\item Let $\rho_\theta$ be the rotation by an angle $\theta$ about the
  chosen axis\pause. We say $\theta$ is ``bad'' if there exists $d_1,d_2\in
  D$ and $n\in \N$ such that $\rho_\theta^n(d_1) = d_2$. \pause
\item The number of bad $\theta$'s is countable. \pause Pick a good angle
  $\bar{\theta}$ and let $\rho = \rho_{\bar{\theta}}$. 
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Transforming $\mathcal{S}_2$ to $\tilde{\mathcal{S}_2}$}


\begin{claim}
For $m\neq n$, the sets $\rho^m(D)$ and $\rho^n(D)$ are disjoint.\pause In
particular, $\Union_{n=1}^\infty \rho^n(D)$ does not contain any point
of $D$.
\end{claim}
\pause
\begin{proof}
  Wlog, assume $m > n$. If $\rho^m(d_1) = \rho^n(d_2)$, then
  $\rho^{m-n}(d_1) = d_2$, which isn't possible by our choice of
  $\rho$. 
\end{proof}

\end{frame}

\begin{frame}
\frametitle{The Partition}

Consider the following partition of $\mathcal{S}$:
$$
A = \Union_{n=0}^\infty \rho^n(D)\qquad B = \mathcal{S}_2 - A
$$
\pause
\begin{eqnarray*}
\rho(A) & =  &\Union_{n=1}^\infty \rho^n(D)\pause = A - D\\\pause
\implies \rho(A) \union B & = & \mathcal{S}_2 - D = \tilde{\mathcal{S}_2}
\end{eqnarray*}
\end{frame}



\subsection{Extending it to the Solid Ball}



\begin{frame}
\frametitle{Doubling a Solid Ball}
\pause
Keep all point along the radius in the same fragment. This allows
doubling $\mathcal{B}_3 - \inbrace{0}$. 
\pause
\begin{itemize}
\item Break up $\mathcal{B}_3$ into $\mathcal{B}_3 - \inbrace{0}$ and
  $\inbrace{0}$. \pause
\item Double the punctured ball to form two copies of $\mathcal{B}_3 -
  \inbrace{0}$. \pause Use $\inbrace{0}$ to complete one copy. \pause
\item As for the other punctured ball, take a circle through the
  origin and use the broken-circle $\equiv$ circle transformation.
\end{itemize}
\pause
That completes the proof of the Banach-Tarski Paradox.
\end{frame}

\section{Conclusion}

\begin{frame}
\frametitle{Last Words}
\begin{itemize}
\item Axiom of Choice isn't as innocent as it looks. \pause
\item Each time you see the AoC in action, ask if you can make the
  choices explicit. \pause
\item The Banach-Tarski Paradox also shows the existence of
  non-measurable sets. \pause

  {\tiny (an easier example of it is the Vitali
  Set. Wiki it)}\pause
\item Any proof of the Banach-Tarski paradox \emph{has} to use the
  Axiom of Choice. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{PJ!}
\begin{block}{What is a nice anagram of Banach-Tarski?}\pause
Banach-Tarski Banach-Tarski
\end{block}

\vspace{1in}

\begin{center}
{\Huge Thank You}
\end{center}
\end{frame}

\end{document}
