\documentclass{beamer}
\mode<presentation>
{
\usetheme{Singapore}
}
\input{preamble}
\renewcommand{\R}{\mathcal{R}}
\renewcommand{\F}{\mathcal{F}}
\title[Integer Multiplication]{Fast Integer Multiplication Using Modular Computation}

\author[Ramprasad Saptharishi]{Anindya De, Piyush P Kurur, Chandan
  Saha,\\ Ramprasad Saptharishi*}

\institute[CMI \& IITK]{
  Chennai Mathematical Institute\\
  and\\
  Indian Institute of Technology, Kanpur\\
  Ph.D Student
}
%% \AtBeginSection[]{
%% \begin{frame}<beamer> 
%% \frametitle{}
%% \tableofcontents[currentsection]
%% \end{frame}
%% }

\begin{document}

\begin{frame}
  \titlepage
\end{frame}

\section{Introduction}
\subsection{Introduction}
\begin{frame}
  \frametitle{Problem Statement}
    Given two $N$ bit integers. Find their product
\end{frame}

\begin{frame}
  \frametitle{History}
  \begin{itemize}
  \item $O\inparen{N^{\log_3 4}}$ by Karatsuba-Offman in 1963. \pause
  \item $O(N^{1+\varepsilon})$ for every $\epsilon>0$ by Toom in
    1963. \pause
  \item Two algorithms by Strassen and Sch\"{o}nhage in 1971:
    \begin{itemize}
    \item $O(N\log N \log\log N \cdots 1\cdot 2^{O(\log^*N)})$
    \item $O(N \log N \log\log N)$
    \end{itemize}\pause
  \item $O(N\log N 2^{O(\log^*N)})$ by F\"{u}rer in 2007. \pause
  \end{itemize}
Main breakthrough: Use of polynomials and FFT. 
\end{frame}

\begin{frame}
  \frametitle{The Basic Philosophy}
  \begin{itemize}
  \item Integer $\Longleftarrow$ Value of Polynomial over a ring. \pause
  \item Poly uniquely determined by sufficient
    evaluations. \pause Evaluations at RoU is the fourier
    transform. \pause
  \item Do a \emph{Fast Fourier Transform}.\pause
  \item Trade-offs:
    \begin{itemize}
    \item Degree vs coefficient size
    \item Small Ring vs ``good'' RoU
    \end{itemize}
  \end{itemize}
\end{frame}

\begin{frame}
\frametitle{Statistics}
\begin{tabular}{lcccc}
 & {\bf Degree} & {\bf Coeff} & {\bf Ring} & {\bf Root of Unity}\\
 & & & & \\
SSa & $\frac{N}{\log N}$ & $\log N$ & ``$\C$'' & $e^{\frac{2\pi
    i}{M}}$\\
 & & & & \\
SSb & $\sqrt{N}$ & $\sqrt{N}$ & $\frac{\Z}{(2^M + 1)\Z}$ & $2$\\
 & & & & \\\pause
F\"{u}r & $\frac{N}{\log^2 N}$ & $\log^2 N$ &
``$\frac{\C[\alpha]}{\alpha^m + 1}$'' & $\rho(\alpha)$\\
 & & & & \\\pause
DKSS & ``$\frac{N}{\log^2 N}$'' & $\log^2 N$ &
$\frac{\Z[\alpha]}{(p^c, \alpha^m + 1)}$ & $\rho(\alpha)$
\end{tabular}
\end{frame}

\section{Generalized FFT}
\subsection{Group Algebras and Characters}
\begin{frame}
\frametitle{Group Algebras}
\begin{definition}
Elements of the group algebra $\C[G]$ are formal sums
$\sum_i{a_i\ket{g_i}}$ where $a_i\in \C$ and $g_i\in G$. Further, they
naturally define addition (pointwise) and multiplication (convolution):
$$
\inparen{\sum a_i\ket{g_i}}\inparen{\sum b_j\ket{h_j}} = \sum_{i,j}a_ib_j\ket{g_i+h_j}
$$
\end{definition}
\pause
Polynomials of degree less than $M$ can be naturally embedded in
$\C\insquar{\frac{\Z}{M\Z}}$. \\
\pause
Elements of the group algebra can also be thought of as a function
from $G$ to $\C$. $\inparen{\sum a_i\ket{g_i}}$ is the map that sends
$g_i$ to $a_i$ for all $i$. 
\end{frame}

\begin{frame}
\frametitle{Characters and the Fourier Transform}

\begin{definition}
A character is a homomorphism from $G$ to $\C$. 
\end{definition}
$$
\ket{\chi} = \sum \chi(g)\ket{g}
$$
\pause
Some properties:
\begin{itemize}
\item $\hat{G}$ is a group, isomorphic to $G$.\pause
\item As vectors, they form an orthogonal set with the natural inner
  product.\pause
\item Inner product with a character corresponds to evaluating the
  polynomial at RoUs.\pause
\end{itemize}

A fourier transform of a vector $\ket{f}$ is just finding
$\braket{\chi}{f}$ for each $\chi\in \hat{G}$. 
\end{frame}

\begin{frame}
\frametitle{Change of Arrows}

Suppose we have a sequence of groups
$$
0 \longrightarrow A \longrightarrow G \longrightarrow B
\longrightarrow 0
$$
then the character groups form a sequence
$$
1 \longleftarrow \hat{A}\longleftarrow \hat{G} \longleftarrow \hat{B}
\longleftarrow 1
$$
\pause
Thus every character $\chi$ of $G$ be thought of as a restriction
$\varphi$ of $A$ and a lift $\lambda$ of $B$. 
\end{frame}
\subsection{Fast Fourier Transform}
\begin{frame}
\frametitle{Fast Fourier Transform}

Fix a set of coset representatives $\inbrace{x_b}_{b\in B}$ of $A$ in
$G$, and a set of coset representatives $\inbrace{\chi_\varphi}_{\varphi \in
  \hat{A}}$.\pause
\begin{itemize}
\item Given a vector $\ket{f}$ write it as $\sum_{a,b}f_{b,a}\ket{x_b
  + a}$. Form vectors $\ket{f_b} = \sum_{a\in A} f_{b,a}\ket{a}$. \pause
\item Take the fourier transforms, over $A$, for each of the
  $\ket{f_b}$'s. This gives coefficients $\braket{\varphi}{f_b}$ for each
  $\varphi\in \hat{A}$ and $b\in B$.\pause
\item Compute the vectors $\ket{f_\varphi} = \sum_{b\in
  B}\overline{\chi}_\phi(x_b)\braket{\phi}{f_b}\ket{b}$. \pause Compute the
  fourier transforms, over $B$ for each of these vectors. This gives
  coefficients $\braket{\lambda}{f_\varphi}$ for each $\lambda\in
  \hat{B}$ and $\varphi \in \hat{A}$. \pause
\item If $\chi$ is a character of $G$, it can be identified by a pair
  $\inparen{\varphi, \lambda}$. Then $\braket{\chi}{f} =
  \braket{\lambda}{f_\varphi}$.
\end{itemize}

\end{frame}
\begin{frame}
\frametitle{What all did that involve?}

\begin{eqnarray*}
\F(G) & = & B\cdot\F(A) + A\cdot\F(B) + G\cdot\inparen{\text{multiplication by
    RoUs}}\\
      &   & \\ \pause
      &   & \text{and if $F_B$ can be further decomposed...}\\
      &   & \\
      & = & \frac{B\log B}{\log A}\F_A + \frac{G\log B}{\log A}\inparen{\text{multiplication by
    RoUs}}
\end{eqnarray*}
\pause
Can we at least make one of the two sums small without making the ring
too large?\pause Yes [F\"{u}r07]

\end{frame}

\subsection{Furer}
\begin{frame}
\frametitle{A Very Quick Sketch of F\"{u}r07}

\begin{itemize}
\item Getting an excellent RoU could be costly, but can we have a
  not-so-bad root and a small ring? \pause
\item We would like the recursive calls to IntMult to be as small as
  possible, can we get $N$ down to $O(log^2N)$?Why not make the
  RoU for $A$ excellent, though it is not so great for the entire
  ring?\pause
\item If $\F_A$ involves degree $m$ polynomials, work over a
  polynomial ring of the form $\frac{R[\alpha]}{\alpha^m +
    1}$. Multiplication by $\alpha$ is just shifts.\pause
\item Manufacture a RoU $\rho(\alpha)$ such that $\rho(\alpha)^{M/m} =
  \alpha$. \pause Multiplication by $\rho(\alpha)$ is arbitrary, but
  some powers will involve just shift operations. \pause
\end{itemize}

Furer's Parameters: Choose powers of two $M =
O\inparen{\frac{N}{\log^2N}}$, $m = O(\log N)$, and set $R = \C$.
\end{frame}

\section{A Modular Algorithm}
\subsection{Modular}
\begin{frame}
\frametitle{Why do we need a modular algorithm?}
\begin{itemize}\pause
\item It gives a discrete approach to a discrete problem. \pause
\item The error analysis is virtually absent. \pause
\item ... and it can be done.
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{What are the issues with adaptation?}
\begin{itemize}
\item We need an $M$-th ``primitive'' root of unity in the ring to
  create $\rho(\alpha)$.\pause
\item A ring of the form $\Z/p^c\Z$ contains only $(p-1)$-th primitive
  roots. Hence $p\equiv 1\bmod M$.\pause
\item Do there exist such primes? Yes, Linnik says there exists one
  within $M^{5.5}$. \pause But $M$ was chosen to be around
  $O(\frac{N}{\log^2N})$\pause... was this necessary?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Why was $M = \frac{N}{\log^2N}$?}  

Recursive calls should be as small as possible;
$O(\log^2N)$. Therefore, the number of coefficients must be
$\frac{N}{\log^2N}$. Is there a way to still keep the RoU required
small?
\pause
\bigskip

Yes, use multivariate polynomials. If there are $k$ variables, degree
in each is enough to be $M =
\inparen{\frac{N}{\log^2N}}^{1/k}$. \pause Choose $k$ such that
$M^{5.5/k} = N^{1 - \epsilon}$ to make the search for $p\equiv 1\bmod
M$ feasible.
\end{frame}

\begin{frame}
\frametitle{The Algorithm}
\begin{itemize}
\item Set appropriate values for constants $k$ and $c$. 
\item Choose powers of two $M = O\inparen{\frac{N}{\log^2N}}^{1/k}$
  and $m = O(\log N)$. 
\item Break up the number into a $k$-variate polynomial over the ring
  $R = \frac{\Z}{p^c\Z}\frac{[\alpha]}{\alpha^m + 1}$.
\item Pick the prime $p$ by brute force, construct $\rho(\alpha)$ and
  use it to compute the fourier tranforms with $G =
  \inparen{\frac{\Z}{M\Z}}^k$ and $A = \inparen{\frac{\Z}{m\Z}}^k$. 
\item Multiply component wise, take the inverse fourier transform,
  evaluate at appropriate points to get the integer product. 
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{``Error Analysis''}
\begin{itemize}
\item Modulo $p^c$ is just working in $\Q_p$ with precision $p^{-c}$.\pause
\item The $p$-adic norm is \emph{non-archimidean} i.e. $\abs{x+y}_p
  \leq max\inbrace{\abs{x}_p,\abs{y}_p}$. Errors don't add up. \pause
\item Just need to choose $c$ to prevent wrap arounds.
\end{itemize}
\end{frame}

\section{Conclusion}
\subsection{Open problem}
\begin{frame}
\frametitle{Open Problems}
\begin{itemize}
\item Obvious open problem: Get rid of the $2^{O(\log^*N)}$ term. \pause
\end{itemize}
\end{frame}

\title[Integer Multiplication]{Thank You}
\author[Ramprasad Saptharishi]{Questions?}
\institute[CMI \& IITK]{}
\date[]{}
\begin{frame}
\titlepage
\end{frame}

\end{document}
