\documentclass{article}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{graphicx}
\usepackage{amsfonts}


\parindent 0pt
\newtheorem{theorem}{Theorem}
\newtheorem{theorem*}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}


\newcommand{\inparan}[1]{\left(#1\right)}


\title{The Complexity of Majority}
\author{Ramprasad Saptharishi\\Chennai Mathematical Institute}
\begin{document}
\maketitle

\begin{abstract}

  We\footnote{This is a summary of my summer project under the
    supervision of Prof V Vinay of the Indian Institute of Science,
    Bangalore} discuss the complexity of majority, through approaches
  from the sorting end and then to Valiant's non-uniform construction
  of a $NC^1$ circuit for majority. We then discuss Ajtai's result
  that shows that majority with polylog advantage can be done in
  $AC^0$.


\end{abstract}

\section{Introduction}

Something needs to be written down

\section{Prelimnaries}

Circuits are a natural notion of a model of computation and the
properties of the circuit structure imposes resource bounds on
computations. 

\begin{definition}$NC^i$ is the class of languages computable by a
  polynomial sized circuit of bounded fan-in and depth $O((\log n)^i)$
  where $n$ is the length of the input.

  $NC = \bigcup NC^i$\\


$AC^i$ is the class of languages computable by a
  polynomial sized circuit of unbounded fan-in and depth $O((\log
  n)^i)$ where $n$ is the length of the input.

  $AC = \bigcup AC^i$
\end{definition}

From the proof that $PARITY\notin AC^0$, we know that $MAJORITY \notin
AC^0$. We want to understand the complexity of majority.

\section{Sorting Approach}

An approach to majority is to try and find a ``small'' circuit that
will sort the input bits, then we can just pick the $n/2$-th bit and
that would give us the majority of the $n$ bits. Hence, a question we
have with us is ``Can we sort in small depth?''

\subsection{Sorting Networks}

Just as circuits are non-adaptive, sorting networks are non-adaptive
rearrangement circuits based on {\em swap gadgets} which run between
two lines, and routing the max and min to corresponding ends of the
gadget. 

Here is an example of a $4$ input sorting network.\\


\includegraphics[width=2in]{im1.png} 
    

The relevant resource bounds here are the number of gadgets and the
depth of the network, which correspond to the size of the circuit and
the depth of the circuit respectively. 

\subsection*{Bubblesort Network}

\includegraphics[width=3in]{im2.png} 



Size = $O(n^2)$

Depth = $O(n)$

\subsection*{Bitonic Sort}

\begin{definition}
A binary sequence is said to be bitonic if there are atmost two
changes in its monotonicity. 
\end{definition}

\begin{lemma} A bitonic sequence can be sorted in depth $O(\log n)$.
\end{lemma}
\begin{proof}
First add $n/2$ swap gadgets in parallel between $i$ and $n/2 + i$ for
all $0\leq i\leq n/2$. This will ensure that at the end of this round,
we now have the property that the first $n/2$ outputs give a bitonic
sequence and so does the last $n/2$. Additionally, we also have that
every bit in the latter half is greater than or equal to every bit in
the first half. 

Hence we can recurse on the two halves separately to give a $O(\log
n)$ depth sorting network for a bitonic sequence. 
\end{proof}

\begin{theorem}[Bitonic Sort]A binary sequence can be sorted with a
  $O(\log^2 n)$ sorting network.
\end{theorem}
\begin{proof}
Recursively sort the first half of the bits in ascending order, and
the second half in descending; this forces a bitonic sequence of
bits. Now use the $O(\log n)$ sorting network in the lemma and sort
it. This gives a $O(\log^2 n)$ sorting network for a binary sequence. 
\end{proof}

\subsection*{Batcher's Sort}

The beauty of this algorithm lies in the merge operation. Given two
sorted sequences, it makes sures that sufficient properties are
enforced so that the merge is a constant depth operation. 

\medskip

{\bf Input:} Two sorted sequences $\{a_1, a_2, \ldots, a_n\}$ and
$\{b_1, b_2, \ldots, b_n\}$

{\bf Merge Algorithm:}
\begin{itemize}
\item Recursively merge the odd indices $\{a_1, a_3, \ldots\}$ and
  $\{b_1, b_3, \ldots\}$ to output a sequence $\{c_1, c_2, \ldots,
  c_n\}$.
\item Similarly do so for the even indices to output $\{d_1, d_2,
  \ldots, d_n\}$. 
\item {\bf Output:} $\{c_1, \min(c_2, d_1), \max(c_2, d_1), \min(c_3,
    d_2), \max(c_3, d_2), \ldots, d_n\}$
\end{itemize}

This clearly takes $O(\log n)$ depth and hence, modulo the correctness
of the algorithm, we have a sorting network of depth $O(\log^2 n)$. 

\begin{theorem} The Merge algorithm correctly merges binary sorted
  sequences.
\end{theorem}
\begin{proof}
  Once we recursively sort the odd and even indices, the number of
  $1$s in the two cases can differ by atmost one in the worst case.
  
  \includegraphics[width=3in]{batcher.png} 

  Hence it's clear that the merge operation runs correctly. 
\end{proof}

Infact, the following lemma would also imply that we have a $O(\log^2
n)$ depth sorting network for arbitrary numbers as well.

\begin{lemma}[$0-1$ Principle] Any sorting network that sorts binary
  inputs correctly, also sorts arbitrary numbers. 
\end{lemma}
\begin{proof}
Suppose the network fails on some sequence arbitrary integers, then
for some $m<n$, the network places $n$ before $m$. Now map all number
less than or equal to $m$ as $0$ and all other numbers to $1$. Since
the network does the same on these inputs, we have a binary sequence
on which it fails. 
\end{proof}

\subsection{Perfect Partitioners}

The general idea to hunt for a $O(\log n)$ depth sorting network is to
somehow split the inputs into two halves where every element of one
half is greater than or equal to every element of the other. Once we
have such a {\em perfect paritioner} we can recurse to get a sorted
sequence in $O(\log n)$ steps. But do we have efficient perfect
partitioners?

\begin{theorem}
An perfect partitioner will have depth $\Omega(\log n)$
\end{theorem}

And hence, a perfect partitioner approach will only give us a
$O(\log^2 n)$ network. 

\begin{proof}
By the $0-1$ principle, it suffices to show this bound for a list of
arbitrary integers. Consider the sequence where the left half has
$n-1$ ones and a three, while the right half has $n-1$ fours and
two. Hence clearly, the perfect partitioner must swap the $2$ and the
$3$. 

Since the $2$ could be placed any any index in the right half, the
comparision tree must have atleast $n/2$ leaves, and hence depth
should be $\Omega(\log n)$. 
\end{proof}

\subsection{AKS Sorters}
Needs to be written down

\section{A Probabilistic Approach}

Valiant's proof that $MAJORITY \in NC^1$ was by constructing a
probabilistic circuit, and amplifying the success probability of the
circuit. Once the error is pushed down further enough, a union bound
would show that there exists one circuit that does not make an error
on any input, thus giving us a non-uniform construction of a circuit
computing $MAJORITY$. 

\subsection{Construction of the Probabilistic Circuit}

The circuit consists of layers of gates computing a majority of $3$
inputs. The inputs to these layers are assigned as follows:
\begin{itemize}
\item With probability $1/2$, assign  $y_i = x_i$. 
\item With probability $1/4$ each, assign $y_i$ as either $0$ or $1$. 
\end{itemize}

Thus if the majority of the bits was $1$, then 
$$
p = \Pr[y_i = 1] = \frac{1}{2} + \frac{1}{n+1}
$$

We would like to amplify this probability of a random bit being a $1$,
this is achieved by the threshold-$3$ gates. 


If $p$ is the probability of an input for a $Th_3$ gate being a $1$,
then the probability that the gate outputs a $1$ with probability
$f(p)=3p^2 - 2p^3$. This function is very useful for amplification
purposes.\\

\includegraphics[width=3in]{graph.png} 

\begin{theorem}
In $O(\log n)$ levels of threshold-$3$ gates, we can reduce the error
probability to $2^{-n}$.
\end{theorem}
\begin{proof}
Let us take the case when $MAJORITY(x_1, \ldots, x_n)=0$, the other
case is symmetric. Then the probability $p$ that an input to the threshold
gate is $1$ is less than $\frac{1}{2}-\frac{1}{n+1}$.

And the mean value theorem tells us that 
$$
f\inparan{\frac{1}{2}-x} = f\inparan{\frac{1}{2}} - xf'(\theta)
$$
for some $\frac{1}{4} \leq \theta \leq \frac{1}{2}$, and for $f(p) =
3p^2 - 2p^3$, 
$$
f\inparan{\frac{1}{2}-x} \leq \frac{1}{2} - \inparan{\frac{9}{8}}x
$$
And hence, with $O(\log n)$ steps, we can push the error down to
$\frac{1}{4}$. \\

And now $f(p) = 3p^2 - 2p^3 \leq 3p^2 \leq  \frac{3}{4}p $, hence
again, with $O(\log n)$ steps we can push this down to $2^{-n}$. 
\end{proof}

And now by the union bound, there must exists a circuit that evaluates
majority correctly on all inputs and hence this shows that $MAJORITY
\in NC^1$.

\section{Greater Advantage}

The general majority corresponds to a inverse polynomial
advantage\footnote{$MAJORITY = 1 \implies \Pr[x_i = 1] \geq \frac{1}{2}
  + \frac{2}{n+1}$}. But suppose we were given the promise that the
advantage was inverse polylog, can we do better? Ajtai-BenOr show that
this can infact be done in $AC^0 \subsetneq NC^1$.\\

{\bf Notation:} We shall say a probabilistic circuit $C$ has an
  advantage $(p,q)$ if 
  \begin{itemize}
  \item $MAJORITY = 0 \implies Pr[C(x)=1]\leq q$
  \item $MAJORITY = 1 \implies Pr[C(x)=1]\geq p$
  \end{itemize}
\bigskip

We want to amplify
$$
\inparan{\frac{1}{2}\inparan{1 + \frac{1}{(\log n)^r}},\frac{1}{2}}
\rightarrow \inparan{1 - \frac{1}{2^n}, \frac{1}{2^n}}
$$

The entire proof will involve a lot of inequalities, they shall be
proved in the appendix. 

\begin{lemma}
If $C$ is a probabilisic circuit with advantage $(p,q)$, then taking
$k$ independant copies changes the advantage as follows:
\begin{itemize}
\item $\wedge$-ing $k$ independant copies of the circuit $C$ changes
  it to $(p^k, q^k)$
\item $\vee$-ing $k$ independant copies of the circuit $C$ changes it
  to $(1 - (1-p)^k, 1- (1-q)^k)$
\end{itemize}
\end{lemma}
\begin{proof}
Straighforward
\end{proof}

\begin{lemma}
In depth $2r$, we can amplify
$$
\inparan{\frac{1}{2}\inparan{1 - \frac{1}{(\log n)^r}},
  \frac{1}{2}}\rightarrow \inparan{\frac{1}{2}\inparan{1 -
    \frac{1}{\log n}}, \frac{1}{2}}
$$
\end{lemma}
\begin{proof}
First we take an $\wedge$ of $4\log n$ copies of the circuit. This
would then amplify it to
\begin{eqnarray*}
\inparan{\frac{1}{2}\inparan{1 + \frac{1}{(\log n)^r}}}^{4\log n} & =
& \frac{1}{2^{4\log n}}\inparan{1 + \frac{1}{(\log n)^r}}^{4\log n}\\
& \leq & \frac{1}{n^4}\inparan{1 + \frac{4\log n}{(\log n)^r}}\\
& = & \frac{1}{n^4}\inparan{1 + \frac{4}{(\log n)^{r-1}}}
\end{eqnarray*}
and the $q$ term would become $\frac{1}{n^4}$. 

Now we take a $\vee$ of $n^4\ln 2$ copies of this circuit, and the
advantage change shall be bounded it using this inequality.

$$
\inparan{1 - \frac{x}{n}}^n\leq e^{-x} \leq 1 - \frac{x}{2}
$$

The left side of the advantage becomes,
\begin{eqnarray*}
1 - \inparan{1 - \frac{1}{n^4}\inparan{1 + \frac{4}{(\log
      n)^{r-1}}}}^{n^4 \ln 2} & \geq & 1 - e^{-\inparan{1 +
    \frac{4}{(\log n)^{r-1}}}\ln 2}\\
= 1 - e^{-\ln 2}e^{\frac{4\ln 2}{(\log n)^{r-1}}} &\geq &1 -
\frac{1}{2}\inparan{1 + \frac{2\ln 2}{(\log n)^{r-1}}}\\
& \geq & \frac{1}{2}\inparan{1 + \frac{1}{(\log n)^{r-1}}}
\end{eqnarray*}

The other side doesn't cause any critical trouble. And with $2$
levels, we have got the degree of the polylog down by $1$, and hence
with $2r$ levels we can get to the desired advantage. 
\end{proof}

\begin{theorem}
In constant depth we can amplify
$$
\inparan{\frac{1}{2}\inparan{1 + \frac{1}{\log n}},
  \frac{1}{2}}\rightarrow \inparan{1 - \frac{1}{2^n}, \frac{1}{2^n}}
$$
\end{theorem}
\begin{proof}
$(1+x)^n \geq 1+nx$, hence
$$
\frac{1}{2}\inparan{1+\frac{1}{\log n}}^{2\log n} \geq \frac{3}{n^2}
$$
Hence by $\wedge$-ing $2\log n$ copies of the circuit, we have
amplified
$$
\inparan{\frac{1}{2}\inparan{1 + \frac{1}{\log n}}, \frac{1}{2}}
\rightarrow \inparan{\frac{3}{n^2}, \frac{1}{n^2}}
$$
\bigskip

Now, $e^{-2x} \leq \inparan{1 - \frac{x}{n}}^n \leq e^{-x}$, and
taking $n^2\ln n$ copies of the circuit under a $\vee$, we have

$$
1 - \inparan{1 - \frac{3}{n^2}}^{n^2\ln n} \geq 1 - e^{-3\ln n} = 1 - \frac{1}{n^3}
$$
and

$$
1 - \inparan{1 - \frac{1}{n^2}}^{n^2\ln n} \leq 1 - e^{-2\ln n} = 1 - \frac{1}{n^2}
$$
thereby using a single $\vee$ gate, we have amplified 
$$
\inparan{\frac{3}{n^2}, \frac{1}{n^2}} \rightarrow \inparan{1 -
  \frac{1}{n^3}, 1 - \frac{1}{n^2}}
$$
\bigskip

And now, to take a $\wedge$ again,
$$
\inparan{1 - \frac{1}{n^3}}^{n^3} \geq e^{-2}
$$
and 
$$
\inparan{1 - \frac{1}{n^2}}^{n^3} \leq e^{-n}
$$
which almost killed the second term while the first is still at a
constant. Thus with a single $\wedge$ gate, we have amplified
$$
\inparan{1 - \frac{1}{n^3}, 1 - \frac{1}{n^2}} \rightarrow
\inparan{e^{-2}, e^{-n}}
$$
\bigskip

Finally, $(1 - e^{-2})^7 \leq \frac{1}{2}$ and hence 
$$
1 - (1 - e^{-2})^{7n} \geq 1 - \frac{1}{2^n}
$$
Whereas,
$$
1 - (1 - e^{-n})^{7n} \leq 1 - (1 - 7ne^{-n}) = 7ne^{-n} \leq \frac{1}{2^n}
$$
Hence with an $\vee$ gate of $7n$ copies, we have hence amplified
$$
(e^{-2}, e^{-n}) \rightarrow \inparan{1 - \frac{1}{2^n}, \frac{1}{2^n}}
$$
and that completes the proof. 
\end{proof}

\subsection{Proof of Inequalities Used}

\begin{lemma}
$$
1 - x \leq e^{-x} \leq 1 - \frac{x}{2}
$$
for all $0\leq x\leq 1$
\end{lemma}
\begin{proof}
It is immediate from the graph of $e^{-x}, 1-x, 1-(x/2)$. As for a
more algebraic proof, 
\begin{eqnarray*}
  e^{-x}& =& 1 - x + \frac{x^2}{2!} - \frac{x^3}{3!} + \ldots\\
  & = & 1 - x + \inparan{\frac{x^2}{2!} - \frac{x^3}{3!}} + \ldots\\
  & = & 1 - x + \text{positive terms}\\
  & \geq & 1 - x\\
  e^{-x} & = &  1 - x + \frac{x^2}{2!} + \text{negative terms}\\
   & \leq & 1 - x + \frac{x^2}{2}\\
   & \leq & 1 - x + \frac{x}{2}\\
   & = & 1 - \frac{x}{2}
\end{eqnarray*}
\end{proof}

\begin{lemma}
$$
e^{-2x} \leq \inparan{1 - \frac{x}{n}}^n \leq e^{-x}
$$
for $0\leq x \leq 1$
\end{lemma}
\begin{proof}
It suffices to show that 
$$
2x \geq n\inparan{-\log\inparan{1- \frac{x}{n}}} \geq x
$$
\begin{eqnarray*}
n\inparan{-\log\inparan{1- \frac{x}{n}}} & = & n\inparan{\frac{x}{n} +
  \frac{\inparan{\frac{x}{n}}^2}{2} +
  \frac{\inparan{\frac{x}{n}}^3}{3} + \ldots}\\
 & \geq & x\\
n\inparan{\frac{x}{n} +
  \frac{\inparan{\frac{x}{n}}^2}{2} +
  \frac{\inparan{\frac{x}{n}}^3}{3} + \ldots} & \leq &
n\inparan{\frac{x}{n} + \frac{x^2}{2n} + \frac{x^3}{2^2n} +
  \frac{x^4}{2^3n} +  \ldots}\\
 & \leq & \frac{x}{1-\frac{x}{2}}\\
 & \leq & 2x
\end{eqnarray*}
\end{proof}
\end{document}