\documentclass{beamer}
\mode<presentation>
{
\usetheme{Boadilla}
}
%\usepackage{times} 
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{ulem}
\usepackage{hyperref}
\usepackage{algorithmic}

\newtheorem{claim}[theorem]{Claim}

\title[Primality Testing]{Primality Testing: The AKS Algorithm}
\author[RP]{Ramprasad Saptharishi}
\institute[CMI]{
  Chennai Mathematical Institute\\
  Third Year B.Sc Mathematics
}
\date[January 17th, 2007]{}

\AtBeginSection[]{
\begin{frame}<beamer> 
\frametitle{Table of Contents}
\tableofcontents[currentsection]
\end{frame}
}

\begin{document}
\maketitle

\section{Introduction}

\begin{frame}
    \frametitle{The Problem}
    
    Given as input a number $n$ in binary. 

    Check if $n$ is prime.\pause

    \bigskip
    
    Input length: $\log n$
    
    Polynomial time $\implies O(\log^k n)$ time

\end{frame}

\begin{frame}
    \frametitle{Introduction}
    \begin{itemize}
      \item Was known to be in $co-NP$ (trivial)
      \item Was known to be in $NP$ (Pratt Certificates)
      \item VERY efficient probabilistic algorithms
      \item Follows from the Extended Riemann Hypothesis
      \item Checking primality critical in CRT and other techniques
    \end{itemize}	
\end{frame}
\section{The Idea}

\begin{frame}
\frametitle{The Distinguisher}

\begin{theorem}
\begin{itemize}
\item If $n$ is prime, then $(X-a)^n = X^n -a\ \ \ \ (\bmod{n})$
\item If $\gcd(a,n)=1$ and $n$ is composite, then $(X-a)^n \neq X^n -a
  \ \ \ \ (\bmod{n})$
\end{itemize}
\end{theorem}

\pause
Algorithm:

Check if $(X-1)^n = X^n - 1 \ \ \ \ (\bmod{n})$ and output accordingly. 

\end{frame}

\begin{frame}
\frametitle{Difficulties}

\begin{enumerate}
\item Computing $(X-1)^n$ takes $n-1$ multiplications (naively)\pause

\bigskip

\item $(X-1)^n$ has $n+1$ terms in it, and computing such a polynomial is
not feasible in $O(\log^k n)$ time.
\end{enumerate}
\end{frame}

\begin{frame}
\frametitle{Getting Around: Difficulty $1$}

{\bf Repeated Squaring:}
\medskip

Compute $p_i = (X-1)^{2^i}$. 

Use the binary representation of $n$ and multiply appropriate $p_i$s
to compute $(X-a)^n$\pause

[Pingala200BC]: Journal Publication in Chandah-Sutra

\end{frame}

\begin{frame}
\frametitle{Getting Around: Difficulty $2$}

$(X-1)^n$ has too many coefficients, $n+1$ of them (exponential in
$\log n$). \pause

\medskip

Evaluate it modulo a polynomial of small degree, go $\bmod (X^r -1)$
for some small $r$ ($O(\log^kn)$). \pause

For compensation, evaluate $(X-a)^n$ with different '$a$'s as
witness. 

\end{frame}

\begin{frame}
\frametitle{The Algorithm: Skeleton}

\pause

\begin{algorithmic}[1]
\STATE \COMMENT{some preliminary tests and choice for $r$, $s$ and
  $a$} \pause
\FOR{$a=1,2,\cdots, s$}\pause
\IF{$(X-a)^n \neq X^n - a \ \ \ (\bmod X^r-1, p)$}
\STATE {\bf declare} {\sc composite}
\ENDIF
\ENDFOR \pause
\STATE {\bf declare} {\sc prime}
\end{algorithmic}
\pause

Things to figure out:
\begin{itemize}
\item What are the preliminary tests?
\item Choice of $r$.
\item Choice of $s$. 
\end{itemize}
\end{frame}

\section{Towards Correctness}

\begin{frame}
\frametitle{The Idea}

\begin{itemize}
\item We want to check if a degree $O(n)$ polynomial is zero ($(X - a)^n
  - (X^n - a)$)\pause
\item We need to restrict resources to $O(\log^k n)$\pause
\item Check modulo a ($O(\log^k n)$) degree polynomial\pause
\item Do it for different $a$'s for control\pause
\item From roots of the small degree polynomials, induce roots for the
  degree $O(n)$ polynomial\pause
\item Show that it has too many roots, and hence show it's identically
  zero\pause
\medskip

\item {\bf Challenge:} From $O(\log^k n)$ polynomials of $O(\log^k n)$,we need to
  induce $O(n)$, exponential blow up in roots. 
\end{itemize}

\end{frame}

\begin{frame}
  \frametitle{Introspective Numbers}

Call a number $m$ introspective if $(X - a)^m = X^m -a
\ \ \ \ (\bmod \ \ X^r - 1, p)$ for all $1\leq a\leq s$.\pause

If $n$ is  a composite number, and $p$ is any  proper prime divisor of
$n$, and that $n$ fails the test for all $a$. We will show that such
problematic $n$ can be for checked earlier by choosing suitable
preliminary tests and values for $r$ and $s$.  \pause
\begin{eqnarray*}
(X -a)^n & = &X^n -a \ \ \ \ (\bmod \ \ X^r -1, p)\\
(X -a)^p & = &X^p - a \ \ \ \ (\bmod \ \ X^r -1, p)
\end{eqnarray*}
the second one coming for free from Fermat's little theorem. 
\end{frame}

\begin{frame}
\frametitle{More Introspective Numbers}

\begin{claim}
If $m_1$ and $m_2$ are introspective numbers, that is for all  $1\leq
a\leq s$
\begin{eqnarray*}
(X -a)^{m_1} & = &X^{m_1} -a\ \ \ \  (\bmod \ \ X^r -1, p)\\
(X -a)^{m_2} & = &X^{m_2} - a\ \ \ \  (\bmod \ \ X^r -1, p)
\end{eqnarray*}
then so is $m_1m_2$
\begin{eqnarray*}
(X -a)^{m_1m_2} & = &X^{m_1m_2} -a\ \ \ \  (\bmod \ \ X^r -1, p)\\
\end{eqnarray*}
\end{claim}
\end{frame}

\begin{frame}
\frametitle{Proof of Claim}

\begin{proof}
\begin{eqnarray*}
(X-a)^{m_2} - (X^{m_2} - a) & = &(X^r - 1)g(X)\\\pause
\implies (X^{m_1} - a)^{m_2} - (X^{m_1m_2} - a) & = & (X^{m_1r} -
1)g(X^{m_1})\\\pause
 & = & 0 \ \ \ \ (\bmod \ \ X^r - 1,p)\\\pause
\implies (X-a)^{m_1m_2} & = & (X^{m_1} - a)^{m_2}\\\pause
& = & X^{m_1m_2} - a \ \ \ \ (\bmod \ \ X^r -1, p)
\end{eqnarray*}
\end{proof}
\end{frame}

\begin{frame}
\frametitle{More Introspective Numbers}

\begin{eqnarray*}
(X -a)^n & = &X^n -a \ \ \ \ (\bmod \ \ X^r -1, p)\\
(X -a)^p & = &X^p - a \ \ \ \ (\bmod \ \ X^r -1, p)
\end{eqnarray*}
Hence, for each $m$ of the form $p^in^j$ we have $(X-a)^m = X^m - a$
for $a = 1,2, \ldots, s$. 
\end{frame}

\begin{frame}
\frametitle{Too many introspective numbers $\implies$ collision}

Let $G$ be the subgroup of $\mathbb{Z}_r$, generated by $n$ and $p$
modulo $r$, and let $t$ be the order of the group. Note that $t \geq
ord_r(n)$. \pause
$$
L = \{ p^in^j | 0\leq i,j\leq \sqrt{t}\}
$$
Note that any $m\in L$ is atmost $n^{2\sqrt t}$. \pause Since $|L| =
(\sqrt{t} + 1)^2 > t$, there exists $m_1 = p^{i_1}n^{j_1}$ and $m_2 =
p^{i_2}n^{j_2}$ such that $m_1 = m_2 + kr$

$$
(X-a)^{m_1} = X^{m_2 + kr} - a = X^{m_2} - a = (X - a)^{m_2}
\ \ \ \ (\bmod \ \ X^r -1, p)
$$
\end{frame}

\begin{frame}
\frametitle{Critical Claim}

\begin{claim}
If $m_1,m_2 \in L$ are such that 
$$
(X - a)^{m_1} = (X - a)^{m_2} \ \ \ \ (\bmod\ \ X^r - 1, p)
$$
for $a = 1, 2, \ldots, s$, then $m_1 = m_2$
\end{claim}
\pause
\bigskip

Once we have this, then $p^{i_1}n^{j_1} = p^{i_2}n^{j_2}$, and this
implies that $n = p^s$. \pause And since we assumed $n$ is composite, $s\geq
2$, and checking if $n$ is a power of a prime is easy and can be added
in the preliminary tests. 

\pause
All that's left now is to choose the parameters that force $m_1 =
m_2$. 
\end{frame}

\section{Conditions required for critical claim}


\begin{frame}
\frametitle{Too many roots $\implies$ zero polynomial}

\begin{theorem}
In field, a non-zero polynomial of degree $d$ has at most $d$ roots. 
\end{theorem}
\pause \medskip

$h(Z) = Z^{m_1} - Z^{m_2}$ has several roots, namely $(X-a)$ for
$a=1,2, \ldots, s$. We want to show that it has more roots than its
degree, thereby forcing $m_1 = m_2$. 

But we need a field first. 

\end{frame}

\begin{frame}
\frametitle{Moving to a field}

Let $\eta$ be a primitive $r$-th root of unity. \pause Then, for $a = 1, 2,
\ldots, s$
$$
(\eta -a)^{m_1} = (\eta - a)^{m_2}
$$
in the field $\mathbb{F}_p(\eta)$, or in other words, $(\eta - a)$ is
a root of the polynomial $h(Z) = Z^{m_1} - Z^{m_2}$. \pause And note that if
$\alpha$ and $\beta$ are roots of $h$, so is $\alpha\beta$. \pause

Thus each element of the set
$$
S = \left\{ \prod_{a=1}^{s} (\eta - a)^{\delta_a} | \delta_a \in \{0,1\}\right\}
$$
is a root of $h$. \pause
Suppose we force that each element in $S$ is
distinct, then we have $2^s$ roots of $h$. \pause

Suppose further that $2^s > n^{2\sqrt{t}}$, that is $s = 2\sqrt{r}\log
n + 1 > 2\sqrt{t}\log n$, then we force $m_1 = m_2$.
\end{frame}

\begin{frame}
\frametitle{Elements of $S$ are distinct}

Suppose we look at 
$$
S_X = \left\{ \prod_{a=1}^{s} (X - a)^{\delta_a} | \delta_a \in \{0,1\}\right\}
$$
what do we need to ensure that they are distinct polynomials of
$\mathbb{F}_p[X]$? \pause The $(X-a)$'s will be distinct if one makes
sure that $a = 1,2,\ldots, s$ do not divide $n$ (and hence $p$) so
that no wrap arounds are possible. This can be added in the
preliminary tests. \pause Since polynomials factor uniquely into
irreducible factors, it follows that $S_X$ has $2^s$ elements.\pause

We now need to push this to show that if $g_1$ and $g_2$ are two
different elements in $S_X$, then $g_1(\eta)$ and $g_2(\eta)$ are
different elements. 

\end{frame}

\begin{frame}
\frametitle{Pushing to $\eta$}

By claim $1$, for every $m = p^in^j$, $g(X)^m = g(X^m) \ \ \ (\bmod
X^r - 1, p)$. \pause Hence if $g_1,g_2$ are two elements of $S_X$ such
that $g_1(\eta) = g_2(\eta)$, then for all $m$ of the form $p^in^j$,
$\eta^m$ is a root of $g = g_1 - g_2 $\pause

$\#$ of values of $\eta^{p^in^j} = $ number of distinct residues of
$p^in^j$ modulo $r$. Hence $g_1 - g_2$ has atleast $t$ roots in
$\mathbb{F}_p(\eta)$. \pause Since these are polynomials of degree
atmost $s$, if we ensure that $t > s = 2\sqrt{r}\log n + 1 >
2\sqrt{t}\log n$, then we force $g_1 = g_2$ in
$\mathbb{F}_p[X]$.\pause And this is true if $t > 4(\log^2n) + 2$
and since $t\geq ord_r(n)$, it's enough to ensure that $ord_r(n) >
4(\log^2n) + 2$. \pause

\medskip

Thus the choice of $r$ is a number such that $ord_r(n) > 4(\log^2n)
+ 2$ and $s = 2\sqrt{r}\log n + 1$, and we are done \pause nearly,
how do we get the $r$?

\end{frame}

\section{Getting hold of the $r$}

\begin{frame}
\frametitle{A Nice Lemma}

\begin{lemma}
The LCM of $1,2,\ldots 2k+1$ is atleast $2^{2k}$
\end{lemma}\pause
\begin{proof}
$$
2^{-2k} \geq \int_0^1[x(1-x)]^kdx \pause= \sum_{i=0}^k \binom{k}{i}
\int_0^1(-1)^i x^{k+i}dx = \sum_{i=0}^k \frac{M_i}{k+i+1} \pause = \frac{M}{L}
$$
where $M$ is an integer and $L$ is the LCM of $k+1, k+2, \ldots,
2k+1$.\pause Since the integral is clearly positive it is atleast
$1/L$, and hence $L \geq 2^{2k}$
\end{proof}
\end{frame}
\begin{frame}
\frametitle{Existance of a small $r$}

Suppose we run through all $r$ till some odd number say $R$ and fail
to get one such that $ord_r(n) > T = 4(\log^2n) + 2$. \pause Then each
$r\leq R$ divides
$$
\prod_{i=0}^T(n^i - 1) \leq n^{T^2}
$$
and hence the LCM of all $r \leq R$ divides it. \pause By the earlier
lemma, we must have $2^{R-1} \leq n^{T^2}$ that is $R \leq T^2\log n
+ 1$. \pause
Hence there is a number $r = O(\log^5n)$ with $ord_r(n)\geq T$. 

\end{frame}
\section{Putting the Pieces Together}

\begin{frame}
\frametitle{The Algorithm for Primality Testing}

\begin{algorithmic}[1]
\IF{$n = a^b$ for $a,b\geq 2$}
\STATE {\bf declare} {\sc composite}
\ENDIF\pause
\STATE Choose $r$ such that $ord_r(n) \geq 4\log^2n + 2$
\STATE Let $s = 2\sqrt{r}\log n + 1$\pause
\IF{any $2\leq a\leq s$ divides $n$}
\STATE {\bf declare} {\sc composite}
\ENDIF\pause
\FOR{$a=1,2,\cdots, s$}
\IF{$(X-a)^n \neq X^n - a \ \ \ (\bmod X^r-1, p)$}
\STATE {\bf declare} {\sc composite}
\ENDIF
\ENDFOR \pause
\STATE {\bf declare} {\sc prime}
\end{algorithmic}
\pause
Running time is $O(\log^{12} n)$, some optimizations has now got it
down to $O(\log^6 n)$. 
\end{frame}

\section{Different Polynomials}

\begin{frame}
\frametitle{Revisiting Difficulty $2$}

\begin{itemize}
\item $(X - 1)^n$ had too many coefficients\pause
\item Solution was to evaluate it modulo a polynomial of small degree
  \pause
\item We compensated for possible error by evaluating $(X-a)^n$ for
  different $a$'s. \pause
\medskip

\item Why not go modulo different polynomials instead?
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Primality and Chinese Remaindering: [AgrawalBiswas]}

\begin{theorem}
For any fixed $r$ and any $t>0$, 
$$
(X + 1)^n = X^n + 1 \ \ \ \ (\bmod \ \ \ n, (X+a)^r - 1) \text{ for }1\leq a
\leq t
$$
if and only if
$$
(X - a)^n = X^n - a \ \ \ \ (\bmod \ \ \ n, X^r - 1) \text{ for }1\leq a
\leq t
$$
\end{theorem}

\pause

[AB99] presents a randomized algorithm for primality and identity
testing using this. After [AKS02], the revised version [AB03] looked
at [AKS02] as a derandomization of [AB99]. 

\end{frame}

\section{Summary and Open Problems}

\begin{frame}
\frametitle{Summary}
\begin{itemize}
\item Simple idea, simple proof
\item A later paper by Manindra converted the entire testing to a
  single polynomial. 
\item $O(\log^6n)$ still not exciting
\item Randomized algorithms do way better
\end{itemize}
\end{frame}


\begin{frame}
\frametitle{Open Problems}
\begin{itemize}
\item More efficient algo
\item Do we actually need to check for various '$a$'s?
\item Lower bounds on primality
\item Exploring tie-ups between [AKS] and [AB]
\end{itemize}
\end{frame}

\title[Primality Testing]{Thank You}
\author[RP]{Slides and \TeX sources are available at \url{~ramprasad/studenttalks/primality/}}
\institute[CMI]{}
\date[January 17th, 2007]{}

\begin{frame}
  \titlepage
\end{frame}

\end{document}