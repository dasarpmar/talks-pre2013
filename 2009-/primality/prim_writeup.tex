\documentclass{article}

\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{amsmath}
\usepackage{amsthm}
\usepackage{algorithmic}
\usepackage{hyperref}

\newtheorem{theorem}{Theorem}
\newtheorem{theorem*}{Theorem}
\newtheorem{corollary}[theorem]{Corollary}
\newtheorem{lemma}[theorem]{Lemma}
\newtheorem{observation}[theorem]{Observation}
\newtheorem{proposition}[theorem]{Proposition}
\newtheorem{definition}[theorem]{Definition}
\newtheorem{claim}[theorem]{Claim}
\newtheorem{fact}[theorem]{Fact}

\title{The AKS Primality Test}
\author{Ramprasad Saptharishi}
\date{}
\begin{document}
\maketitle
\section{Why am I doing this?}

I do not have any excuses but my last talk on Primality was very fast,
it might have been very hard on some of the students who were not used
to the proof techniques or algebra used inside it. Hopefully this
write-up should fill in the hand-wavings/voids in the talk. 

\section{Introduction}

Primality has been a problem studied for centuries. It appears in
numerous areas, and finding efficient algorithms to test for primality
is fundamental techniques in complexity theory and algorithms. 

It wasn't taken much by surprise when primality was shown to be in
polynomial time since we already knew quite a lot about it. 

\begin{enumerate}
\item Primality is in $co-NP$: A language $L$ is in $co-NP$ if for
  $x\notin L$, there exists a small witness for this. In the primality
  case, if a number $n$ was not a prime number, then you have a
  divisor which is a small witness for $n$ being composite. 
\item Primality is in $NP$: Shown by Pratt, slightly non-trivial. Very
  clever idea that uses recursion. 
\item {\bf Very} efficient randomized algorithms. 
\item Followed from the extended reimann hypothesis. 
\end{enumerate}
and a lot more. 

For a long time people had been trying to solve the problem and
Agrawal, Kayal and Saxena answered in the affirmative. 

\section{The Problem Statement}

{\bf Primality:} Input is a number $n$ in {\em binary} (hence length
of input is only $O(\log n)$). Decide whether $n$ is a prime number or
not. 

Remember that if we are talking about polynomial time algorithms, it's
polynomial in the length of the input. Hence our algorithm should run
in time $O(\log^kn)$ and hence our resources have such bounds. 

In general, all primality tests use a distinguisher test. There is
some identity that works for primes but not for composites, use that
identity to test for primality. 

For example, we know by Fermat's little theorem that $a^{p-1} \equiv
1\bmod p$ for $p$ a prime. There are some moronic Carmicheal numbers
that satisfy this identity as well hence this immediately doesn't give
a distinguisher. 

\medskip
The distinguisher used in this algorithm is the following:
\begin{theorem}
A number $n$ is prime if and only if
$$
(X-a)^n = X^n - a \bmod n
$$
\end{theorem}

Hence all we need to do is check this identity for
$a=1$. Unfortunately, Godel prize winning papers are not that
simple. There are some difficulties. 

\begin{enumerate}
\item $(X-1)^n$ takes (naively) $n-1$ multiplications.

This however can be tackled in a clever way. For each $0\leq k\leq
\log n$, compute $(X-1)^{2^k}$. Now look at the binary representation
of $n$. If $n = b_0 + b_12 + b_22^2 + \cdots b_l2^l$, then
$$
(X-1)^n = (X-1)^{b_0}((X-1)^2)^{b_1}((X-1)^{2^2})^{b_2}\cdots((X-1)^{2^l})^{b_l}
$$
which is just a product of $l$ terms among those you had computed
earlier. This $(X-1)^n$ can be computed efficiently. 

\item There could potentially be $n$ terms in $(X-1)^n$, checking if
  it was equal to $X^n - a$ is clearly infeasible in polylog time. 

Tackling this difficulty is the heart of the algorithm. The solution
is to go modulo a small polynomial say $X^r - 1$. This essentially
means you take $X^r = 1$ in the polynomial, thus forcing its degree to
be bounded by $r-1$. 

For example:
$$
(x^{12} + 23x^{10} + x^3 + x^2 + 5) = 1 + 23x + 1 + x^2 + 5\ \ \ (\bmod
x^3 -1)
$$

Hence instead of checking if $(X-1)^n = X^n - a \bmod n$, we will
check $(X-a)^n = X^n - a\ \ \ (\bmod X^r - 1, n)$ for some small $r$
and for different $a$'s. 
\end{enumerate}

Thus our algorithm sketch is the following:

\begin{algorithmic}[1]
\STATE \COMMENT{some preliminary tests and choice for $r$, $s$ and
  $a$}
\FOR{$a=1,2,\cdots, s$}
\IF{$(X-a)^n \neq X^n - a \ \ \ (\bmod X^r-1, p)$}
\STATE {\bf declare} {\sc composite}
\ENDIF
\ENDFOR 
\STATE {\bf declare} {\sc prime}
\end{algorithmic}

We still have to figure out the preliminary tests and the choice for
$r$ and $s$. 

\section{Preventing composite $n$ from passing the test}

Suppose $n$ was composite and $p$ was a prime divisor of $n$, we need
to impose sufficient conditions on $n$ to fail the test at some
step. Here we have this nice idea of 'introspective numbers' as they
call it. 

Call a number $m$ introspective (definition depends on the parameters)
if for all $a = 1,2, \cdots, s$
$$
(X-a)^m = X^m - a\ \ \ (\bmod X^r - 1, p)
$$

Suppose some stupid composite $n$ passed the test, then certainly $n$
was introspective since if $(X-a)^n = X^n -a\ \ (\bmod X^r - 1, n)$
then certainly also mod $p$. And further, from Fermat's little
theorem, we have $(X-a)^p = X^p - a$ for free. 

Hence:
\begin{eqnarray*}
  (X-a)^n & = & X^n - a \ \ (\bmod X^r -1, p)\\
  (X-a)^p  & = & X^p - a \ \ (\bmod X^r -1, p)
\end{eqnarray*}

And we have the nice claim, saying that introspective numbers are
multiplicative. 

\begin{claim}
If $m_1$ and $m_2$ are introspective, that is for all $1\leq a\leq s$
\begin{eqnarray*}
  (X-a)^{m_1} & = & X^{m_1} - a \ \ (\bmod X^r -1, p)\\
  (X-a)^{m_2}  & = & X^{m_2} - a \ \ (\bmod X^r -1, p)
\end{eqnarray*}
then so is $m_1m_2$
$$
(X-a)^{m_1m_2} = X^{m_1m_2} - a\ \ (\bmod X^r - 1, p)
$$
\end{claim}
\begin{proof}
  I assume the proof was clear in the talk, you may revisit the slides
  if not. 
\end{proof}

From this we have that if $n$ and $p$ are introspective, then so is
every number of the form $p^in^j$. 
\medskip

Since $X^r = 1$, it makes sense to just look at the residues of
$p^in^j \bmod r$. Thus consider $\mathbb{Z}_r^{\star}$, the
multiplicative group modulo $r$ and let its size be $t$. 

Look at the set
$$
L = \{p^in^j\ :\ 0\leq i,j\leq \sqrt{t}\}
$$
The number of elements in the set is $(\sqrt{t} + 1)^2 > t$ and hence
by pigeon hole principle, there exist two number $m_1 =
p^{i_1}n^{j_1}$ and $m_2 = p^{i_2}n^{j_2}$ with $m_1 = m_2 + kr$. This
would hence force that $(X-a)^{m_1} = (X-a)^{m_2} \ \ (\bmod\ X^r -1,
p)$ as $X^r = 1 = X^{kr}$. 

\medskip
Suppose from $(X-a)^{m_1} = (X-a)^{m_2}$ we were to force that
$m_1=m_2$, then we have joy! If $m_1 = m_2$, $n$ must be $p^k$ for
some $k$, and this can be checked easily. (for each $2\leq k\leq \log
n$, do a binary search on $a = 2,\cdots, n$ to check if any $a^b =
n$). 
\smallskip

We now just need to ensure that $m_1 = m_2$ is forced. 

\section{Required to force $m_1=m_2$}

We have $(X-a)^{m_1} = (X-a)^{m_2}\ \ (\bmod\ X^r - a, p)$ for all $a$
in the range. This is just saying the polynomial $h(z) = z^{m_1} -
z^{m_2}$ has $(X-a)$ for different $a$ as roots of it. 

Note that the degree of $h$ is atmost $\max(m_1, m_2)$ and since
$m_1,m_2\in L$, $p^in^j \leq n^{2i}\leq n^{2\sqrt{t}}$ since $\sqrt{t}
$ was the bound on $i$ and $j$. We will now go on to show that $h$ has
more than $n^{2\sqrt{t}}$ many roots. 

\medskip

Look at the primitive $r$-th root of unity, $\eta$. Since $X$ is
essentially $\eta$ since we are going mod $X^r - 1$, it just means
that $(\eta - a)$ are roots of $h$. Now note that if $(\eta - a)$ and
$(\eta - b)$ are roots of $h(z) = z^{m_1} - z^{m_2}$, then so is
$(\eta - a)(\eta-b)$. Hence, every element of
$$
S = \left\{\prod_{a = 1}^s (\eta - a)^{\delta_a}\ : \ \delta\in \{0,1\}\right\}
$$
Things would be fantastic if we show that every element of $S$ is
distinct, then we have $2^s$ roots of $h$. Then all we need to ensure
is that $2^s > n^{2\sqrt{t}}$. 

Recall that $t = |\mathbb{Z}_r^{\star}| \leq r-1$. With some playing
around with the inequalities, it's easy to see that $s = 2\sqrt{r}\log
n + 1$ works for the required inequality $2^s > n^{2\sqrt{t}}$. Since
the polynomial $h$ has more roots than its degree, it has to be zero,
hence $m_1 = m_2$. 

\bigskip

Now we need to show that the elements of $S$ are distinct. Let us
instead look at a different set,
$$
S = \left\{\prod_{a = 1}^s (X - a)^{\delta_a}\ : \ \delta\in \{0,1\}\right\}
$$
Are these elements distinct? Just look at the $(X-a)$'s, when will
$(X-a) = (X-b)$? This can happen only when $a = b\bmod p$ which means
$(a-b)= 0\bmod p$ or $p$ divides $(a-b)$. Since we are only looking at
$(X-a)$ where $1\leq a\leq s$ with some small $s$, if $p$ divides
$a-b$, then $p$ certainly has to be less than $s$. Hence if we ensure
that $n$ has no small prime divisors like $p$, we can be certain that
each $(X-a)$ is a distinct element. Hence, just add to the prelimnary
tests to check for small divisors (for each $2\leq a\leq s$, check if
$a$ divides $n$). 

Now any polynomial in $S_X$ factorizes as linear factors of the form
$(X-a)$, and since each of these factors are distinct, and the
factorization is unique, the polynomials as such are distinct. Hence
$S_X$ has $2^l$ elements. It's now left to show that for two different
polynomials $g_1$ and $g_2$ in $S_X$, $g_1(\eta) \neq g_2(\eta)$. 
\medskip

Suppose for two different $g_1$ and $g_2$, if $g_1(\eta) = g_2(\eta)$.
Recall our earlier claim on introspective numbers, $g(X)^m = g(X^m)$
for all $m$ of the form $p^in^j$. Hence if $\eta$ is a root of $g_1 -
g_2$, then so is $\eta^{p^in^j}$.
\smallskip

$\eta^r$ is anyway $1$, hence we need to look at the different roots
that $\eta^{p^in^j}$ generate for different $i$ and $j$. Since $\eta$
was chosen to be a primitive root, $\eta^{p^in^j} = \eta^{p^in^j \bmod
  r}$ and there are as many values of the exponent as there are
residues of $p^in^j$ modulo $r$. Hence equivalently we need to ask,
how many residues modulo $r$ do  $p^in^j$ generate? OK, we don't know
anything about this prime number $p$, but if the order of $n$ modulo
$r$ was $k$, then $p^in^j$ has atleast $k$ different residues. 

$g_1 - g_2$ is a polynomial of degree atmost $s$, and if we choose
$ord_r(n) > s$, then we have more roots than the degree, thus $g_1 =
g_2$. Hence we just need to choose an $r$ such that the degree of $n$
modulo $r$ is greater than $s$. Recall that $s = 2\sqrt{t}\log n + 1
\leq 2\sqrt{r}\log n + 1$(since the size of the multiplicative group
modulo $r$, which is equal to $t$, has to be less than $r$). 

Since $t \leq ord_r(n)$, and we want $ord_r(n) \geq 2\sqrt{t}\log n +
1$, the inequalities can be solved easily. An $r$ such that $ord_r(n)
\geq 4\log^2n + 2$ is a safe choice. 
\medskip

Then there is some technical detail of saying that a such an  $r$ exists
within a small range and can be found easily. I'm omitting that part,
you could see the presentation for the details on that. 

Finally, all the pieces put together.

\begin{algorithmic}[1]
\STATE \COMMENT{as needed after forcing $m_1=m_2$}
\IF{$n = a^b$ for $a,b\geq 2$}
\STATE {\bf declare} {\sc composite}
\ENDIF
\STATE Choose $r$ such that $ord_r(n) \geq 4\log^2n + 2$
\STATE Let $s = 2\sqrt{r}\log n + 1$
\STATE \COMMENT{to make sure there are no small divisors, preventing wrap arounds}
\IF{any $2\leq a\leq s$ divides $n$}
\STATE {\bf declare} {\sc composite}
\ENDIF
\FOR{$a=1,2,\cdots, s$}
\STATE\COMMENT{the distinguisher test}
\IF{$(X-a)^n \neq X^n - a \ \ \ (\bmod X^r-1, p)$}
\STATE {\bf declare} {\sc composite}
\ENDIF
\ENDFOR
\STATE {\bf declare} {\sc prime}
\end{algorithmic}

\section{Conclusion}

The other open problems part can be seen in the presentation. My
apologies for going too fast in the talk, I realised only half way
through that I had not been clear, it was already late by then. I
tried to make up at the end of it, but wasn't convinced that I did a
good job. I hope this write-up makes things a clearer, I'd be
delighted to receive any comments/suggestions or to answer any doubts
in this. 

Thank you. 

\end{document}